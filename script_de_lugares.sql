INSERT INTO lugares (nombre, descripcion, latitud, longitud, created_at, updated_at) VALUES ("CATEDRAL DE MORELIA", "<p>La catedral de Morelia es un recinto religioso sede de la arquidiócesis de Morelia de la Iglesia católica en México</p>","19.7023107", "-101.192377", '2020-06-05 01:23:15', '2020-06-05 01:23:15'),
("FUENTE DE LAS TARASCAS", "<p>Se trata de la fuente más famosa de Morelia: una escultura elaborada en bronce que representa a tres mujeres purépechas con el torso descubierto, cargando una cesta de frutos</p>","19.7028598", "-101.18247344203935", '2020-06-05 01:23:15', '2020-06-05 01:23:15'),

("ACUEDUCTO DE MORELIA", "<p>Este imponente acueducto levantado en el siglo XVIII para surtir de agua a Morelia es una de las obras civiles más importantes y mejor conservadas de la ciudad. Fue edificado para sustituir el rudimentario canal construido como sistema de suministro en el siglo XVI y está formado por 253 arcos de medio punto, con alturas que superan los 9 metros</p>","19.6985386", "-101.1773078", '2020-06-05 01:23:15', '2020-06-05 01:23:15'),

("CASA NATAL DE MORELOS", "<p>El patriota mexicano José María Morelos y Pavón da su nombre a la ciudad de Morelia, en la que nació el 30 de septiembre de 1765 cuando la localidad todavía se llamaba Valladolid. La casona natal del héroe, de una sola planta y dos patios interiores, fue originalmente de estilo barroco cuando fue construida en el siglo XVII, pero una reconstrucción de 1888 la hizo neoclásica</p>","19.7007999", "-101.1925246", '2020-06-05 01:23:15', '2020-06-05 01:23:15'),

("PARQUE ZOOLÓGICO BENITO JUÁREZ", "<p>El Zoo Benito Juárez de Morelia es uno de los más grandes de Latinoamérica por cantidad de especies representadas y número de ejemplares.</p>","19.68614315", "-101.19379693807173", '2020-06-05 01:23:15', '2020-06-05 01:23:15'),

("MUSEO REGIONAL MICHOACANO", "<p>Este museo inaugurado en 1886 en el Colegio de San Nicolás es el más antiguo del país excluyendo Ciudad de México. Funciona desde 1916 en una hermosa casona de estilo barroco construida en el siglo XVIII, que fue residencia de notables familias, incluyendo la de Ana Huarte, esposa del emperador Agustín de Iturbide</p>","19.701716", "-101.194363", '2020-06-05 01:23:15', '2020-06-05 01:23:15'),

("TEATRO OCAMPO", "<p>Ofreció su primera pieza el día de año nuevo de 1830 con el nombre de Teatro Coliseo en una función de la compañía Salgado con la primera actriz Amada Plata. El 3 de junio de 1861, en medio de una función, llegó la noticia del fusilamiento del dirigente liberal Melchor Ocampo y los asistentes decidieron renombrar el teatro. La construcción neobarroca ha sido objeto de varias modificaciones, la última en el año 2000 para mejorar la acústica de su majestuosa sala de 403 butacas</p>","19.7041608", "-101.19396803694454", '2020-06-05 01:23:15', '2020-06-05 01:23:15'),

("CONSERVATORIO DE LAS ROSAS", "<p>Esta institución musical abrió sus puertas en 1743, siendo el primer conservatorio de América. El edificio del centro histórico donde funciona actualmente el conservatorio, de señorial sobriedad, data de 1595 y ha sido sucesivamente Convento Dominico de Santa Catalina de Siena, Colegio de Santa Rosa, Escuela Superior de Música Sacra y finalmente Conservatorio de Las Rosas.</p>","19.704845", "-101.1942658", '2020-06-05 01:23:15', '2020-06-05 01:23:15'),

("PLAZA DE ARMAS", "<p>La Plaza de Armas es la plaza principal de la ciudad, en ella se encuentra la Catedral de Morelia, un kiosko y bancas</p>","19.702348100000002", "-101.19364541151207", '2020-06-05 01:23:15', '2020-06-05 01:23:15'),

("MERCADO DE DULCES", "<p>Los dulces típicos mexicanos son una delicia y en Morelia pueden encontrar un mercado especializado en ellos. En el mercado pueden encontrar desde las tradicionales morelianas, ates, hasta cocadas, tamarindos, cajeta, etc.</p>","19.703888499999998", "-101.19585151081478", '2020-06-05 01:23:15', '2020-06-05 01:23:15'),

("CALLEJÓN DEL ROMANCE", "<p>Este es el único callejón del Centro Histórico, rodeado de casas coloniales y una agradable pileta, los enamorados llenan de romanticismo el espacio decorado con flores. El callejón tiene un fragmento de romance escrito por el poeta Lucas Ortiz.</p>","19.703375", "-101.1819525", '2020-06-05 01:23:15', '2020-06-05 01:23:15'),

("BOSQUE CUAUHTÉMOC", "<p>El Bosque Cuauhtémoc es el mas grande de la ciudad y de los mas importantes, es ideal para meditar, hacer ejercicio y pasar un agradable rato dando un paseo. En su interior hay jardines que tienen un orquideario, juegos infantiles y además también se encuentran los museos de Historia Natural y Arte Contemporáneo</p>","19.69902655", "-101.18006915401347", '2020-06-05 01:23:15', '2020-06-05 01:23:15'),

("CASA DE LAS ARTESANÍAS", "<p>Casa de las Artesanías en Morelia, ubicada en una de las construcciones más antiguas de la ciudad, es un lugar ideal para admirar la vasta producción artesanal del estado. Cuenta con un museo dividido en siete regiones de producción artesanal y con una tienda anexa en donde podrás adquirir varios de estos artículos. </p>","19.701559", " -101.188748", '2020-06-05 01:23:15', '2020-06-05 01:23:15'),

("PLANETARIO LIC. FELIPE RIVERA", "<p>El Planetario de Morelia cuenta con el único proyector MARK IV de Karl Zeiss funcionando en el país, su espectacular cielo permite observar más de 8,000 estrellas en el cielo. Cuenta con 360 cómodas butacas.</p>","19.68486265", "-101.18328022015348", '2020-06-05 01:23:15', '2020-06-05 01:23:15'),

("CALZADA FRAY ANTIONIO DE SAN MIGUEL", "<p>Conocida popularmente como “Calzada de Guadalupe” o “Calzada de San Diego”, la Calzada de Fray Antonio de San Miguel, tiene su origen en el siglo XVII y servía para comunicar a la ciudad virreinal con una capilla dedicada a la Virgen de Guadalupe.</p>","19.7018959", "-101.1800531", '2020-06-05 01:23:15', '2020-06-05 01:23:15'),

("ESTADIO JOSÉ MARÍA MORELOS Y PAVÓN", "<p>El Estadio Morelos es un estadio de fútbol ubicado sobre el periférico Paseo de la República, en el sector Independencia, al noroeste de la ciudad de Morelia, Michoacán, México. Fue sede del equipo de fútbol profesional Monarcas Morelia de la Primera División de México hasta 2020.</p>","19.718823399999998", "-101.23356835015583", '2020-06-05 01:23:15', '2020-06-05 01:23:15'),

("CASA DE LA CULTURA", "<p>La Casa de la Cultura de Morelia es un centro cultural comunitario localizado en la ciudad de Morelia Michoacán, México y cumple 40 años de existencia en 2017.</p>","19.706324600000002", "-101.19179338856907", '2020-06-05 01:23:15', '2020-06-05 01:23:15'),

("PLAZA ESPACIO LAS AMÉRICAS", "<p>Centro comercial con más de 170 establecimientos de las mejores marcas de moda, entretenimiento, restaurantes y servicios complementarios</p>","19.6890601", "-101.1583579754473", '2020-06-05 01:23:15', '2020-06-05 01:23:15'),

("CENTRO COMERCIAL PASEO ALTOZANO", "<p>Un centro comercial de vanguardia que ofrece múltiples servicios, tiendas y lugares de esparcimiento. Un punto de encuentro para ir de compras, disfrutar una comida o divertirse en un evento.</p>","19.674477", "-101.163492", '2020-06-05 01:23:15', '2020-06-05 01:23:15'),

("INSTITUTO TECNÓLOGICO DE MORELIA", "<p>Instituto Tecnológico de Morelia es una institución educativa pública de educación superior, que forma parte del Sistema Nacional de Institutos Tecnológicos de México</p>","19.721495", " -101.185502", '2020-06-05 01:23:15', '2020-06-05 01:23:15'),

("UNIVERSIDAD MICHOACANA DE SAN NICOLAS DE HIDALGO", "<p>Es una institución académica pública del estado de Michoacán, con sede en Morelia, Michoacán, México. Sus antecedentes datan de 1540, cuando Vasco de Quiroga fundó, en Pátzcuaro, el Colegio de San Nicolás Obispo, trasladado en 1580 a Valladolid</p>","19.6892946", "-101.20662594648806", '2020-06-05 01:23:15', '2020-06-05 01:23:15'),

("ITESM CAMPUS MORELIA", "<p>El Instituto Tecnológico de Estudios Superiores de Monterrey, Campus Morelia es uno de los campus del Instituto Tecnológico y de Estudios Superiores de Monterrey. Está ubicado al sureste de la ciudad mexicana de Morelia, en la comunidad de Jesús del Monte.</p>","19.656341", " -101.164526", '2020-06-05 01:23:15', '2020-06-05 01:23:15'),

("UNAM CAMPUS MORELIA", "<p>Mediante la creación de las Unidades Académicas Foráneas en la ciudad de Morelia, la UNAM inició el esfuerzo de propiciar el flujo de cuadros científicos hacia la provincia, de una manera organizada. El 11 de noviembre de 1996 se inauguró el Campus de la UNAM en Morelia.</p>","19.647932", " -101.22964", '2020-06-05 01:23:15', '2020-06-05 01:23:15'),

("UVAQ MORELIA", "<p>La Universidad Vasco de Quiroga es una institución educativa privada de nivel medio superior y superior con sede en la ciudad de Morelia,</p>","19.6756399", "-101.1803981", '2020-06-05 01:23:15', '2020-06-05 01:23:15'),

("LA SALLE MORELIA", "<p>La Universidad La Salle Morelia es una institución de Educación Superior Internacional e Innovadora, con responsabilidad social, sentido humano y visión</p>","19.761111149999998", "-101.18049402755648", '2020-06-05 01:23:15', '2020-06-05 01:23:15'),

("CAFÉ MICHELENA", "<p>Café Michelena es una fusión de cafetería y librería que ofrece desayunos, comidas y cenas. El concepto de este lugar invita a disfrutar de la variedad de bebidas calientes junto con un delicioso pan tradicional en compañía de un buen libro. </p>","19.7018013", "-101.1933777", '2020-06-05 01:23:15', '2020-06-05 01:23:15'),

("CAFÉ EUROPA", "<p>En CAFÉ EUROPA tenemos el compromiso de mantener siempre la más alta calidad y selección de los mejores granos de café, distinguibles por su pureza, frescura y tostado, haciendo de nuestra actividad una tradición de la que nuestro personal pueda sentirse orgulloso al satisfacer el gusto de nuestros consumidores</p>","19.7030474", "-101.1928756", '2020-06-05 01:23:15', '2020-06-05 01:23:15'),

("BANDERA MONUMENTAL DE MORELIA", "<p>Como su nombre lo indica, en el lugar se encuentra un asta de grandes dimensiones en la que ondea la bandera mexicana como símbolo de orgullo nacional. Gracias a la altura que tiene la explanada se utiliza como mirador para poder observar gran parte de la ciudad de Morelia. Con frecuencia se utiliza para realizar algunos eventos.</p>","19.6764038", "-101.1739985", '2020-06-05 01:23:15', '2020-06-05 01:23:15'),

("COLEGIO SAN NICOLAS DE HIDALGO", "<p>El edificio del Colegio de San Nicolás de Hidalgo, ubicado en el corazón de la ciudad de Morelia, fue edificado en el siglo XVII durante la época virreinal funcionando desde entonces como institución educativa, primero con el nombre de Real Colegio de San Nicolás Obispo y posteriormente como Colegio de San Nicolás de Hidalgo. Fue uno de los centros de estudios más importantes de la Nueva España. Actualmente alberga una escuela preparatoria dependiente de la Universidad Michoacana.</p>","19.703236", " -101.194805", '2020-06-05 01:23:15', '2020-06-05 01:23:15'),

("BIBLIOTECA PÚBLICA UNIVERSITARIA", "<p>El edificio que aloja hoy a la Biblioteca Pública Universitaria originalmente fue el templo, que durante la época del Virreinato de la Nueva España, formaba parte del conjunto conventual de la Compañía de Jesús en Valladolid, hoy Morelia, el cual comprendía también el anexo Colegio de San Francisco Javier, hoy Centro Cultural Clavijero</p>","19.703247", "-101.195515", '2020-06-05 01:23:15', '2020-06-05 01:23:15'),

("ANTIGUO PALACIO DE JUSTICIA", "<p>El antiguo Palacio de Justicia, en Morelia, es un bello edificio de fachada afrancesada y estilo ecléctico que hoy funciona como Museo y Archivo Histórico del Poder Judicial de Michoacán. Este fue el primer fue el primer Museo Histórico del Poder Judicial en México y fue inaugurado el 18 de mayo de 2004.</p>","19.7017041", "-101.19408836811968", '2020-06-05 01:23:15', '2020-06-05 01:23:15')
;
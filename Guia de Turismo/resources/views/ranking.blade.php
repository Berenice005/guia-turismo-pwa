<head>
@laravelPWA
</head>
<nav class="nav flex">
<h1>IT GROUP SOLUTION</h1>
  <div class="user-navigation flex">
    <div class="user-block pointer">
      <div id="personal-border" class="user-border flex absolute bright-blue"></div>
      <div class="user-container flex align-center relative">
        <p class="messages-notification absolute bright-blue">1</p>
        <img class="avatar" src="https://image.freepik.com/vector-gratis/trofeo-dorado-realista-espacio-texto_48799-1062.jpg" />
        <div class="user-info flex column justify-center">
          <p class="user-username">Ranking de Guías</p>
          <p class="user-score">2020 ITGS</p>
        </div>

      </div>


  </div>
</nav>

<div class="content">

  <div class="leaderboard flex column wrap">
    <div class="leaderboard-table flex column">
      <div class="leaderboard-header flex column grow">
<!--
          <div class="filter-by flex grow wrap">
            <div class="time-filter flex grow">
              <div class="row-button pointer row-button--active align-center">Today</div>
              <div class="row-button pointer align-center">This week</div>
              <div class="row-button pointer align-center">All-time</div>
            </div>
            <div class="subject-filter flex grow">
              <div class="table-tab pointer flex grow justify-center align-center tab-active">
                <svg class="menu-link-icon" fill="#FFFFFF" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                  <path d="M12 12c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm0 2c-2.67 0-8 1.34-8 4v2h16v-2c0-2.66-5.33-4-8-4z"/>
                </svg>
                Users</div>
              <div class="table-tab pointer flex grow justify-center align-center"> <svg class="menu-link-icon" fill="#4a4a4a" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                <path d="M16 11c1.66 0 2.99-1.34 2.99-3S17.66 5 16 5c-1.66 0-3 1.34-3 3s1.34 3 3 3zm-8 0c1.66 0 2.99-1.34 2.99-3S9.66 5 8 5C6.34 5 5 6.34 5 8s1.34 3 3 3zm0 2c-2.33 0-7 1.17-7 3.5V19h14v-2.5c0-2.33-4.67-3.5-7-3.5zm8 0c-.29 0-.62.02-.97.05 1.16.84 1.97 1.97 1.97 3.45V19h6v-2.5c0-2.33-4.67-3.5-7-3.5z"/>
                </svg>
                Teams</div>
            </div>
          </div>
          -->

          <div class="leaderboard-row flex align-center row--header" style="border-radius: 0 !important;">
            <div class="row-position">Posición</div>
            <div class="row-collapse flex align-center">
              <div class="row-user--header">Guía</div>
              <div class="row-team--header">Creador</div>
              <div class="row-rank--header">Horas</div>
            </div>
            <div class="row-calls">Puntos</div>
          </div>
        </div>


    <div class="leaderboard-body flex column grow">
      <div class="leaderboard-row flex align-center">
        <div class="row-position">1</div>
        <div class="row-collapse flex align-center">
          <div class="row-caller flex">
            <img class="avatar" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/225473/ruffalo-avatar.png" />
            <div class="row-user">Guía del turista.</div>
          </div>
          <div class="row-team">Pedro Juarez</div>
          <div class="row-rank">100 hras.</div>
        </div>
        <div class="row-calls">1567</div>
      </div>

      <div class="leaderboard-row flex row-alt align-center">
        <div class="row-position">2</div>
        <div class="row-collapse flex align-center">
          <div class="row-caller flex">
            <img class="avatar" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/225473/danny-avatar.png" />
            <p class="row-user">Guía del estudihambre.</p>
          </div>
          <div class="row-team">Carlos Estrada</div>
          <div class="row-rank">48 hras.</div>
        </div>
        <div class="row-calls">567</div>
      </div>

      <div class="leaderboard-row flex align-center">
        <div class="row-position">3</div>
        <div class="row-collapse flex align-center">
          <div class="row-caller flex">
            <img class="avatar" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/225473/robert-avatar.png" />
            <div class="row-user">Guía del Romance.</div>
          </div>
          <div class="row-team">Jimena López.</div>
          <div class="row-rank">24 hras.</div>
        </div>
        <div class="row-calls">231</div>
      </div>

      <div class="leaderboard-row flex row-alt align-center">
        <div class="row-position">4</div>
        <div class="row-collapse flex align-center">
          <div class="row-caller flex">
            <img class="avatar" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/225473/killer_mike.png" />
            <div class="row-user">Guía del lector.</div>
          </div>
          <div class="row-team">Luis Sanchez</div>
          <div class="row-rank">72 hras.</div>
        </div>
        <div class="row-calls">144</div>
      </div>

      <div class="leaderboard-row flex align-center">
        <div class="row-position">5</div>
        <div class="row-collapse flex align-center">
          <div class="row-caller flex">
            <img class="avatar" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/225473/corbyn.png" />
            <div class="row-user">Guía de los 60's</div>
          </div>
          <div class="row-team">Carla Treviño</div>
          <div class="row-rank">48 Hras.</div>
        </div>
        <div class="row-calls">126</div>
      </div>
<!-- 
      <div class="leaderboard-footer flex align-center">
  Página 1 de 2 <a class="footer-btn pointer">Siguiente</a> 25 de 37 items
      </div>
      -->

    </div>
  </div>

  </div>
  
</div>

<footer class="flex column align-center justify-center">
  <p>Ranking de Guias de turismo en Morelia</p>
</footer>

<style type="text/css">
* {
  padding: 0;
  margin: 0;
  box-sizing: border-box;
  -webkit-font-smoothing: antialiased !important;
  -moz-font-smoothing: antialiased !important;
  -o-font-smoothing: antialiased !important;
  text-rendering: optimizeLegibility !important;
}

body {
  font-family: "soleil", "roboto", sans-serif;
  overflow-x: hidden;
  background-color: #f1f1f1;
  color: #040404;
}

.justify-center{
  justify-content: center;
}

.menu-icon {
  flex-grow: 1;
  padding: 0 58px;
  border-left: 1px solid #FFF;
  border-right: 1px solid #FFF;
  border-bottom: 1px solid #FFF;
  background-color: #FFF;
  z-index: 2;
}

.flex{
  display: flex;
}

.column{
  flex-direction: column;
}

.search-bar{
  padding: 40px 10vw 0;
  
}

.search{
  border: 0;
  padding: 8px 20px;
  font-size: 18px;
  font-family: "soleil", "roboto", sans-serif;
  border: 1px solid #bdbbbb;
}

.active {
  background-color: #eeeeee !important;
  border-left: 1px solid #e2e2e2 !important;
  border-right: 1px solid #e2e2e2 !important;
  border-bottom: 1px solid #e2e2e2 !important;
}

.nav {
  align-items: center;
  color: #040404;
  font-size: 22px;
  font-weight: 500;
  justify-content: space-between;
  background-color: #FFFFFF;
  padding: 0 10vw;
  -webkit-box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.16);
  -moz-box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.16);
  box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.16);
}

.menu-block{
  margin-left: -1px;
}

.menu-block,
.user-block {
  position: relative;
}

.pointer{
  cursor: pointer;
}

.user-container {
  padding: 10px 15px;
  border-left: 1px solid #FFFFFF;
  border-right: 1px solid #FFFFFF;
  border-top: 1px solid #FFFFFF;
  border-bottom: 1px solid #FFFFFF;
  background-color: #FFFFFF;
  z-index: 2;
  overflow-x: hidden;
}

.user-border {
  height: 5px;
  flex-grow: 1;
  width: 100%;
  top: -8px;
  left: 0px;
  z-index: 3;
}

.absolute{
  position: absolute;
}

.relative{
  position: relative;
}

.user-menu {
  right: 0;
  top: -62px;
  z-index: 1;
}

.menu-link-icon {
  margin-right: 8px;
}

.menu-link-notification {
  border-radius: 100%;
  height: 16px;
  width: 16px;
  color: #FFFFFF;
  font-size: 10px;
  top: 8px;
  left: 13px;
  padding-right: 1px;
}

.dropup-icon,
.dropdown-icon {
  margin-left: 10px;
}

.bright-blue{
  background-color: #32B8DF;
}


.messages-notification {
  border-radius: 100%;
  height: 20px;
  width: 20px;
  color: #FFFFFF;
  text-align: center;
  font-size: 12px;
  top: 14px;
  left: 5px;
}

.icon {
  width: 70px;
  height: 70px;
  padding: 22px;
}

.user-info {
  margin-left: 15px;
}

.user-username {
  font-size: 18px;
  font-weight: 500;
}

.user-score {
  font-size: 14px;
  color: #989898;
  font-weight: 300;
  margin-top: -3px;
}

.shadow{
  -webkit-box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
  -moz-box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
  -ms-box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
  box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
}

.wrap{
  flex-wrap: wrap;
}

.grow{
  flex-grow: 1;
}

.leaderboard {
  padding: 40px 10vw;
}

.leaderboard-header{
  border-radius: 4px 4px 0 0;
}

.footer-btn{
  padding: 8px 12px;
  margin: 0 15px;
  background-color: #FFF;
  border-radius: 4px;
  color: #32b8df;
  border: 1px solid #3498db;
}

.leaderboard-footer{
  padding: 20px 3vw;
  background-color: #32b8df;
  border: 1px solid #0196c1;
  color: #FFF;
}

.leaderboard-row{
  flex-grow: 1;
  justify-content: space-between;
  background-color: #FFFFFF;
  padding: 10px 3vw;
  border-left: 1px solid #CCC;
  border-right: 1px solid #CCC;
}

.row-caller{
  align-items: center;
}

.row-user{
  margin-left: 30px;
  width: 185px;
}

.row-user--header{
  width: 275px;
}

.row-position{
  width: 55px;
  text-align: center;
}

.row-calls{
  width: 55px;
  text-align: right;
}

.leaderboard-header > .leaderboard-row:first-of-type {
  border-radius: 4px 4px 0 0 !important;
}

.leaderboard-header > .leaderboard-row{
  background-color: #32b8df;
  color: #FFFFFF;
  border-left: 1px solid #0196c1;
  border-bottom: 1px solid #0196c1;
  border-right: 1px solid #0196c1;
}

.leaderboard-row:last-of-type {
  border-radius: 0 0 4px 4px;
}

.align-center{
  align-items: center;
}

.row-collapse{
  flex-grow: 1;
  justify-content: space-around;
}

.row-alt{
  background-color: #F2f2f2;
}

.row-button{
  padding: 12px 40px 10px 40px;
  color: #080808;
  border-top: 1px solid #CCC;
  border-right: 1px solid #CCC;
  background-color: #FFFFFF;
}

.table-tab:hover,
.row-button:hover{
  background-color: #f2f2f2;
}

.row-button--active{
  border-right: 1px solid #0196c1;
  border-left: 1px solid #0196c1;
  border-top: 1px solid #0196c1;
}

.filter-row{
  padding: 0;
}

.table-tab{
  padding: 10px;
  border-top: 1px solid #CCC;
  border-right: 1px solid #CCC;
  background-color: #FFFFFF;
}

.filter-by{
  border-bottom: 1px solid #0196c1;
  background-color: rgba(0,0,0,0);
}

.row-button--active,
.tab-active{
  background-color: #32B8DF;
  color: #FFFFFF;
  border-left: 1px solid #0196c1;
  border-top: 1px solid #0196c1;
  border-right: 1px solid #0196c1;
}

.row-button--active:hover,
.tab-active:hover{
  background-color: #20add6;
}

.row--header{
  padding: 20px 3vw;
}

.row-rank--header,
.row-team--header,
.row-rank,
.row-team{
  width: 16.67%;
}

.avatar{
  border-radius: 100%;
  width: 60px;
  height: 60px;
}

.wordmark {
  height: 65px;
  margin: 8px 0;
  cursor: pointer;
}

.content{
  min-height: 100vh;
}

footer {
  background-color: #3498db;
  padding: 25px 200px;
  color: #FFFFFF;
}

.nav-link {
  background-color: #EEE;
  font-size: 18px;
  font-weight: 300;
  color: #4a4a4a;
  padding: 10px 25px 10px 20px;
  border-left: 1px solid #E2E2E2;
  border-right: 1px solid #E2E2E2;
  border-bottom: 1px solid #E2E2E2;
  position: relative;
  letter-spacing: -0.5px;
  z-index: 1;
}

.nav-link:hover{
  background-color: #c7c7c7;
  border-left: 1px solid #bdbdbd;
  border-right: 1px solid #bdbdbd;
  border-bottom: 1px solid #bdbdbd;
}

.nav-links {
  position: absolute;
  background-color: #EEE;
  left: 0;
  top: -62px;
  width: 100%;
}

@media (max-width: 1050px) {
  .row-rank--header,
  .row-team--header{
    display: none;
  }
  
  .row-collapse{
    flex-direction: column;
  }
  
  .row-caller{
    margin-bottom: -37px;
    align-items: flex-start;
  }
  
  .row-rank,
  .row-team{
    width: 275px;
    padding-left: 90px;
    font-size: 12px;
  }
  
  .wordmark {
    height: 40px;
  }
  
  .nav {
    padding: 0 10vw;
  }
  
}

@media (max-width: 800px) {
  
  .row-button:last-of-type{
    border-right: 0;
  }
  
  .filter-by{
    flex-direction: column;
  }
  
  .table-tab {
    border-top: 1px solid #E2E2E2;
    border-left: 0px;
  }
 
  .tab-active {
    border-top: 1px solid #0196c1;
  }
  
  .row-button{
    flex-grow: 1;
    text-align: center;
  }
  
  .user-container {
    padding: 4px 15px;
  }
  .user-menu {
    top: -96px;
  }
  .dropdown-icon,
  .dropup-icon,
  .user-info {
    display: none;
  }
  .nav {
    height: 70px;
  }
  .menu-icon {
    height: 70px;
    width: 82px;
    padding: 23px;
  }
  .nav-link {
    border: 1px solid #E2E2E2;
    padding: 8px 25px;
  }
  .active-icon {
    background-color: #EEE;
    -webkit-box-shadow: inset 0px 0px 10px 2px rgba(0, 0, 0, 0.1);
    -moz-box-shadow: inset 0px 0px 10px 2px rgba(0, 0, 0, 0.1);
    box-shadow: inset 0px 0px 10px 2px rgba(0, 0, 0, 0.1);
  }
}

.animate-border {
  animation: animate-border 0.3s ease-in-out 1;
  animation-fill-mode: forwards;
}

.remove-border {
  animation: remove-border 0.3s ease-in-out 1;
  animation-fill-mode: forwards;
}

@keyframes animate-border {
  0% {
    transform: translateY(0);
  }
  100% {
    transform: translateY(7px);
  }
}

@keyframes remove-border {
  0% {
    transform: translateY(7px);
  }
  100% {
    transform: translateY(0px);
  }
}

.menu-slide-up {
  animation: menu-slide-up 0.3s ease-in-out 1;
  animation-fill-mode: forwards;
}

.menu-slide-down {
  animation: menu-slide-down 0.3s ease-in-out 1;
  animation-fill-mode: forwards;
}

@keyframes menu-slide-down {
  0% {
    transform: translateY(0px);
  }
  100% {
    transform: translateY(144px);
  }
}

@keyframes menu-slide-up {
  0% {
    transform: translateY(144px);
  }
  100% {
    transform: translateY(0px);
  }
}

  </style>

  <script>
  $(document).ready(function() {
  
  var border = $('#personal-border');
  var userMenu = $('.user-menu');
  var userBlock = $('.user-block');

  $('.dropup-icon').hide();

  $('.menu-icon').click(function() {
    if ($('.nav-links').hasClass('menu-slide-down')) {
      $('.nav-links').removeClass('menu-slide-down');
      $('.nav-links').addClass('menu-slide-up');
    } else {
      $('.nav-links').removeClass('menu-slide-up');
      $('.nav-links').addClass('menu-slide-down');
    }
    $(this).toggleClass('active');
    $('.menu-icon').toggleClass('active-icon');
  });

  userBlock.click(function() {
    $('.dropup-icon').toggle();
    $('.dropdown-icon').toggle();
    $('.user-container').toggleClass('active');
    if (userMenu.hasClass('menu-slide-down')) {
      userMenu.removeClass('menu-slide-down');
      userMenu.addClass('menu-slide-up');
    } else {
      userMenu.removeClass('menu-slide-up');
      userMenu.addClass('menu-slide-down');
    }
  });

  userBlock.mouseenter(function() {
    border.removeClass('remove-border');
    border.addClass('animate-border');
  });

  userBlock.mouseleave(function() {
    border.removeClass('animate-border');
    border.addClass('remove-border');
  });
  
  $('.menu-icon').mouseenter(function() {
    $('#menu-border').removeClass('remove-border');
    $('#menu-border').addClass('animate-border');
  });

  $('.menu-icon').mouseleave(function() {
    $('#menu-border').removeClass('animate-border');
    $('#menu-border').addClass('remove-border');
  });
});
  </script>
@extends('layouts.admin')
@section('content')
<div class="content">

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.edit') }} {{ trans('cruds.estadoGuium.title_singular') }}
                </div>
                <div class="panel-body">
                    <form method="POST" action="{{ route("admin.estado-guia.update", [$estadoGuium->id]) }}" enctype="multipart/form-data">
                        @method('PUT')
                        @csrf
                        <div class="form-group {{ $errors->has('guia') ? 'has-error' : '' }}">
                            <label class="required" for="guia_id">{{ trans('cruds.estadoGuium.fields.guia') }}</label>
                            <select class="form-control select2" name="guia_id" id="guia_id" required>
                                @foreach($guias as $id => $guia)
                                    <option value="{{ $id }}" {{ ($estadoGuium->guia ? $estadoGuium->guia->id : old('guia_id')) == $id ? 'selected' : '' }}>{{ $guia }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('guia'))
                                <span class="help-block" role="alert">{{ $errors->first('guia') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.estadoGuium.fields.guia_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('estado') ? 'has-error' : '' }}">
                            <label class="required">{{ trans('cruds.estadoGuium.fields.estado') }}</label>
                            <select class="form-control" name="estado" id="estado" required>
                                <option value disabled {{ old('estado', null) === null ? 'selected' : '' }}>{{ trans('global.pleaseSelect') }}</option>
                                @foreach(App\EstadoGuium::ESTADO_SELECT as $key => $label)
                                    <option value="{{ $key }}" {{ old('estado', $estadoGuium->estado) === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('estado'))
                                <span class="help-block" role="alert">{{ $errors->first('estado') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.estadoGuium.fields.estado_helper') }}</span>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-danger" type="submit">
                                {{ trans('global.save') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>



        </div>
    </div>
</div>
@endsection
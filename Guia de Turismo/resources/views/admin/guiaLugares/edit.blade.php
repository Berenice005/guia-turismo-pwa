@extends('layouts.admin')
@section('content')
<div class="content">

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.edit') }} {{ trans('cruds.guiaLugare.title_singular') }}
                </div>
                <div class="panel-body">
                    <form method="POST" action="{{ route("admin.guia-lugares.update", [$guiaLugare->id]) }}" enctype="multipart/form-data">
                        @method('PUT')
                        @csrf
                        <div class="form-group {{ $errors->has('guia') ? 'has-error' : '' }}">
                            <label class="required" for="guia_id">{{ trans('cruds.guiaLugare.fields.guia') }}</label>
                            <select class="form-control select2" name="guia_id" id="guia_id" required>
                                @foreach($guias as $id => $guia)
                                    <option value="{{ $id }}" {{ ($guiaLugare->guia ? $guiaLugare->guia->id : old('guia_id')) == $id ? 'selected' : '' }}>{{ $guia }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('guia'))
                                <span class="help-block" role="alert">{{ $errors->first('guia') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.guiaLugare.fields.guia_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('lugar') ? 'has-error' : '' }}">
                            <label class="required" for="lugar_id">{{ trans('cruds.guiaLugare.fields.lugar') }}</label>
                            <select class="form-control select2" name="lugar_id" id="lugar_id" required>
                                @foreach($lugars as $id => $lugar)
                                    <option value="{{ $id }}" {{ ($guiaLugare->lugar ? $guiaLugare->lugar->id : old('lugar_id')) == $id ? 'selected' : '' }}>{{ $lugar }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('lugar'))
                                <span class="help-block" role="alert">{{ $errors->first('lugar') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.guiaLugare.fields.lugar_helper') }}</span>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-danger" type="submit">
                                {{ trans('global.save') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>



        </div>
    </div>
</div>
@endsection
@extends('layouts.admin')
@section('content')
<div class="content">

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.show') }} {{ trans('cruds.lugare.title') }}
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="form-group">
                            <a class="btn btn-default" href="{{ route('admin.lugares.index') }}">
                                {{ trans('global.back_to_list') }}
                            </a>
                        </div>
                        <table class="table table-bordered table-striped">
                            <tbody>
                                <tr>
                                    <th>
                                        {{ trans('cruds.lugare.fields.id') }}
                                    </th>
                                    <td>
                                        {{ $lugare->id }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.lugare.fields.nombre') }}
                                    </th>
                                    <td>
                                        {{ $lugare->nombre }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.lugare.fields.foto') }}
                                    </th>
                                    <td>
                                        @if($lugare->foto)
                                            <a href="{{ $lugare->foto->getUrl() }}" target="_blank">
                                                <img src="{{ $lugare->foto->getUrl('thumb') }}" width="50px" height="50px">
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.lugare.fields.descripcion') }}
                                    </th>
                                    <td>
                                        {!! $lugare->descripcion !!}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.lugare.fields.latitud') }}
                                    </th>
                                    <td>
                                        {{ $lugare->latitud }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.lugare.fields.longitud') }}
                                    </th>
                                    <td>
                                        {{ $lugare->longitud }}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="form-group">
                            <a class="btn btn-default" href="{{ route('admin.lugares.index') }}">
                                {{ trans('global.back_to_list') }}
                            </a>
                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div>
</div>
@endsection
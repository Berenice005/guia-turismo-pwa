@extends('layouts.admin')
@section('content')
<div class="content">

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.show') }} {{ trans('cruds.promocione.title') }}
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="form-group">
                            <a class="btn btn-default" href="{{ route('admin.promociones.index') }}">
                                {{ trans('global.back_to_list') }}
                            </a>
                        </div>
                        <table class="table table-bordered table-striped">
                            <tbody>
                                <tr>
                                    <th>
                                        {{ trans('cruds.promocione.fields.id') }}
                                    </th>
                                    <td>
                                        {{ $promocione->id }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.promocione.fields.nombre') }}
                                    </th>
                                    <td>
                                        {{ $promocione->nombre }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.promocione.fields.lugar') }}
                                    </th>
                                    <td>
                                        {{ $promocione->lugar->nombre ?? '' }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.promocione.fields.descripcion') }}
                                    </th>
                                    <td>
                                        {!! $promocione->descripcion !!}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.promocione.fields.fotopromo') }}
                                    </th>
                                    <td>
                                        @if($promocione->fotopromo)
                                            <a href="{{ $promocione->fotopromo->getUrl() }}" target="_blank">
                                                <img src="{{ $promocione->fotopromo->getUrl('thumb') }}" width="50px" height="50px">
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.promocione.fields.fechainicio') }}
                                    </th>
                                    <td>
                                        {{ $promocione->fechainicio }}
                                    </td>
                                </tr>
                                <tr>
                                    <th>
                                        {{ trans('cruds.promocione.fields.fechatermina') }}
                                    </th>
                                    <td>
                                        {{ $promocione->fechatermina }}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="form-group">
                            <a class="btn btn-default" href="{{ route('admin.promociones.index') }}">
                                {{ trans('global.back_to_list') }}
                            </a>
                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div>
</div>
@endsection
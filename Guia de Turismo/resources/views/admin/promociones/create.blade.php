@extends('layouts.admin')
@section('content')
<div class="content">

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.create') }} {{ trans('cruds.promocione.title_singular') }}
                </div>
                <div class="panel-body">
                    <form method="POST" action="{{ route("admin.promociones.store") }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group {{ $errors->has('nombre') ? 'has-error' : '' }}">
                            <label class="required" for="nombre">{{ trans('cruds.promocione.fields.nombre') }}</label>
                            <input class="form-control" type="text" name="nombre" id="nombre" value="{{ old('nombre', '') }}" required>
                            @if($errors->has('nombre'))
                                <span class="help-block" role="alert">{{ $errors->first('nombre') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.promocione.fields.nombre_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('lugar') ? 'has-error' : '' }}">
                            <label class="required" for="lugar_id">{{ trans('cruds.promocione.fields.lugar') }}</label>
                            <select class="form-control select2" name="lugar_id" id="lugar_id" required>
                                @foreach($lugars as $id => $lugar)
                                    <option value="{{ $id }}" {{ old('lugar_id') == $id ? 'selected' : '' }}>{{ $lugar }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('lugar'))
                                <span class="help-block" role="alert">{{ $errors->first('lugar') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.promocione.fields.lugar_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('descripcion') ? 'has-error' : '' }}">
                            <label for="descripcion">{{ trans('cruds.promocione.fields.descripcion') }}</label>
                            <textarea class="form-control ckeditor" name="descripcion" id="descripcion">{!! old('descripcion') !!}</textarea>
                            @if($errors->has('descripcion'))
                                <span class="help-block" role="alert">{{ $errors->first('descripcion') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.promocione.fields.descripcion_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('fotopromo') ? 'has-error' : '' }}">
                            <label for="fotopromo">{{ trans('cruds.promocione.fields.fotopromo') }}</label>
                            <div class="needsclick dropzone" id="fotopromo-dropzone">
                            </div>
                            @if($errors->has('fotopromo'))
                                <span class="help-block" role="alert">{{ $errors->first('fotopromo') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.promocione.fields.fotopromo_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('fechainicio') ? 'has-error' : '' }}">
                            <label class="required" for="fechainicio">{{ trans('cruds.promocione.fields.fechainicio') }}</label>
                            <input class="form-control datetime" type="text" name="fechainicio" id="fechainicio" value="{{ old('fechainicio') }}" required>
                            @if($errors->has('fechainicio'))
                                <span class="help-block" role="alert">{{ $errors->first('fechainicio') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.promocione.fields.fechainicio_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('fechatermina') ? 'has-error' : '' }}">
                            <label class="required" for="fechatermina">{{ trans('cruds.promocione.fields.fechatermina') }}</label>
                            <input class="form-control datetime" type="text" name="fechatermina" id="fechatermina" value="{{ old('fechatermina') }}" required>
                            @if($errors->has('fechatermina'))
                                <span class="help-block" role="alert">{{ $errors->first('fechatermina') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.promocione.fields.fechatermina_helper') }}</span>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-danger" type="submit">
                                {{ trans('global.save') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>



        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function () {
  function SimpleUploadAdapter(editor) {
    editor.plugins.get('FileRepository').createUploadAdapter = function(loader) {
      return {
        upload: function() {
          return loader.file
            .then(function (file) {
              return new Promise(function(resolve, reject) {
                // Init request
                var xhr = new XMLHttpRequest();
                xhr.open('POST', '/admin/promociones/ckmedia', true);
                xhr.setRequestHeader('x-csrf-token', window._token);
                xhr.setRequestHeader('Accept', 'application/json');
                xhr.responseType = 'json';

                // Init listeners
                var genericErrorText = `Couldn't upload file: ${ file.name }.`;
                xhr.addEventListener('error', function() { reject(genericErrorText) });
                xhr.addEventListener('abort', function() { reject() });
                xhr.addEventListener('load', function() {
                  var response = xhr.response;

                  if (!response || xhr.status !== 201) {
                    return reject(response && response.message ? `${genericErrorText}\n${xhr.status} ${response.message}` : `${genericErrorText}\n ${xhr.status} ${xhr.statusText}`);
                  }

                  $('form').append('<input type="hidden" name="ck-media[]" value="' + response.id + '">');

                  resolve({ default: response.url });
                });

                if (xhr.upload) {
                  xhr.upload.addEventListener('progress', function(e) {
                    if (e.lengthComputable) {
                      loader.uploadTotal = e.total;
                      loader.uploaded = e.loaded;
                    }
                  });
                }

                // Send request
                var data = new FormData();
                data.append('upload', file);
                data.append('crud_id', {{ $promocione->id ?? 0 }});
                xhr.send(data);
              });
            })
        }
      };
    }
  }

  var allEditors = document.querySelectorAll('.ckeditor');
  for (var i = 0; i < allEditors.length; ++i) {
    ClassicEditor.create(
      allEditors[i], {
        extraPlugins: [SimpleUploadAdapter]
      }
    );
  }
});
</script>

<script>
    Dropzone.options.fotopromoDropzone = {
    url: '{{ route('admin.promociones.storeMedia') }}',
    maxFilesize: 2, // MB
    acceptedFiles: '.jpeg,.jpg,.png,.gif',
    maxFiles: 1,
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    params: {
      size: 2,
      width: 4096,
      height: 4096
    },
    success: function (file, response) {
      $('form').find('input[name="fotopromo"]').remove()
      $('form').append('<input type="hidden" name="fotopromo" value="' + response.name + '">')
    },
    removedfile: function (file) {
      file.previewElement.remove()
      if (file.status !== 'error') {
        $('form').find('input[name="fotopromo"]').remove()
        this.options.maxFiles = this.options.maxFiles + 1
      }
    },
    init: function () {
@if(isset($promocione) && $promocione->fotopromo)
      var file = {!! json_encode($promocione->fotopromo) !!}
          this.options.addedfile.call(this, file)
      this.options.thumbnail.call(this, file, '{{ $promocione->fotopromo->getUrl('thumb') }}')
      file.previewElement.classList.add('dz-complete')
      $('form').append('<input type="hidden" name="fotopromo" value="' + file.file_name + '">')
      this.options.maxFiles = this.options.maxFiles - 1
@endif
    },
    error: function (file, response) {
        if ($.type(response) === 'string') {
            var message = response //dropzone sends it's own error messages in string
        } else {
            var message = response.errors.file
        }
        file.previewElement.classList.add('dz-error')
        _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
        _results = []
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            node = _ref[_i]
            _results.push(node.textContent = message)
        }

        return _results
    }
}
</script>
@endsection
@extends('layouts.admin')
@section('content')
<div class="content">

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('global.create') }} {{ trans('cruds.experiencium.title_singular') }}
                </div>
                <div class="panel-body">
                    <form method="POST" action="{{ route("admin.experiencia.store") }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group {{ $errors->has('usuario') ? 'has-error' : '' }}">
                            <label class="required" for="usuario_id">{{ trans('cruds.experiencium.fields.usuario') }}</label>
                            <select class="form-control select2" name="usuario_id" id="usuario_id" required>
                                @foreach($usuarios as $id => $usuario)
                                    <option value="{{ $id }}" {{ old('usuario_id') == $id ? 'selected' : '' }}>{{ $usuario }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('usuario'))
                                <span class="help-block" role="alert">{{ $errors->first('usuario') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.experiencium.fields.usuario_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('comentario') ? 'has-error' : '' }}">
                            <label for="comentario">{{ trans('cruds.experiencium.fields.comentario') }}</label>
                            <textarea class="form-control ckeditor" name="comentario" id="comentario">{!! old('comentario') !!}</textarea>
                            @if($errors->has('comentario'))
                                <span class="help-block" role="alert">{{ $errors->first('comentario') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.experiencium.fields.comentario_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('lugar') ? 'has-error' : '' }}">
                            <label class="required" for="lugar_id">{{ trans('cruds.experiencium.fields.lugar') }}</label>
                            <select class="form-control select2" name="lugar_id" id="lugar_id" required>
                                @foreach($lugars as $id => $lugar)
                                    <option value="{{ $id }}" {{ old('lugar_id') == $id ? 'selected' : '' }}>{{ $lugar }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('lugar'))
                                <span class="help-block" role="alert">{{ $errors->first('lugar') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.experiencium.fields.lugar_helper') }}</span>
                        </div>
                        <div class="form-group {{ $errors->has('calificacion') ? 'has-error' : '' }}">
                            <label class="required">{{ trans('cruds.experiencium.fields.calificacion') }}</label>
                            <select class="form-control" name="calificacion" id="calificacion" required>
                                <option value disabled {{ old('calificacion', null) === null ? 'selected' : '' }}>{{ trans('global.pleaseSelect') }}</option>
                                @foreach(App\Experiencium::CALIFICACION_SELECT as $key => $label)
                                    <option value="{{ $key }}" {{ old('calificacion', '') === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
                                @endforeach
                            </select>
                            @if($errors->has('calificacion'))
                                <span class="help-block" role="alert">{{ $errors->first('calificacion') }}</span>
                            @endif
                            <span class="help-block">{{ trans('cruds.experiencium.fields.calificacion_helper') }}</span>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-danger" type="submit">
                                {{ trans('global.save') }}
                            </button>
                        </div>
                    </form>
                </div>
            </div>



        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function () {
  function SimpleUploadAdapter(editor) {
    editor.plugins.get('FileRepository').createUploadAdapter = function(loader) {
      return {
        upload: function() {
          return loader.file
            .then(function (file) {
              return new Promise(function(resolve, reject) {
                // Init request
                var xhr = new XMLHttpRequest();
                xhr.open('POST', '/admin/experiencia/ckmedia', true);
                xhr.setRequestHeader('x-csrf-token', window._token);
                xhr.setRequestHeader('Accept', 'application/json');
                xhr.responseType = 'json';

                // Init listeners
                var genericErrorText = `Couldn't upload file: ${ file.name }.`;
                xhr.addEventListener('error', function() { reject(genericErrorText) });
                xhr.addEventListener('abort', function() { reject() });
                xhr.addEventListener('load', function() {
                  var response = xhr.response;

                  if (!response || xhr.status !== 201) {
                    return reject(response && response.message ? `${genericErrorText}\n${xhr.status} ${response.message}` : `${genericErrorText}\n ${xhr.status} ${xhr.statusText}`);
                  }

                  $('form').append('<input type="hidden" name="ck-media[]" value="' + response.id + '">');

                  resolve({ default: response.url });
                });

                if (xhr.upload) {
                  xhr.upload.addEventListener('progress', function(e) {
                    if (e.lengthComputable) {
                      loader.uploadTotal = e.total;
                      loader.uploaded = e.loaded;
                    }
                  });
                }

                // Send request
                var data = new FormData();
                data.append('upload', file);
                data.append('crud_id', {{ $experiencium->id ?? 0 }});
                xhr.send(data);
              });
            })
        }
      };
    }
  }

  var allEditors = document.querySelectorAll('.ckeditor');
  for (var i = 0; i < allEditors.length; ++i) {
    ClassicEditor.create(
      allEditors[i], {
        extraPlugins: [SimpleUploadAdapter]
      }
    );
  }
});
</script>

@endsection
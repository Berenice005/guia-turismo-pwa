@extends('layouts.admin')
@section('content')
<div class="content">
    @can('guium_create')
        <div style="margin-bottom: 10px;" class="row">
            <div class="col-lg-12">
                <a class="btn btn-success" href="{{ route("admin.guia.create") }}">
                    {{ trans('global.add') }} {{ trans('cruds.guium.title_singular') }}
                </a>
                <button class="btn btn-warning" data-toggle="modal" data-target="#csvImportModal">
                    {{ trans('global.app_csvImport') }}
                </button>
                @include('csvImport.modal', ['model' => 'Guium', 'route' => 'admin.guia.parseCsvImport'])
            </div>
        </div>
    @endcan
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('cruds.guium.title_singular') }} {{ trans('global.list') }}
                </div>
                <div class="panel-body">
                    <table class=" table table-bordered table-striped table-hover ajaxTable datatable datatable-Guium">
                        <thead>
                            <tr>
                                <th width="10">

                                </th>
                                <th>
                                    {{ trans('cruds.guium.fields.id') }}
                                </th>
                                <th>
                                    {{ trans('cruds.guium.fields.nombre') }}
                                </th>
                                <th>
                                    {{ trans('cruds.guium.fields.usuario') }}
                                </th>
                                <th>
                                    {{ trans('cruds.user.fields.name') }}
                                </th>
                                <th>
                                    &nbsp;
                                </th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>



        </div>
    </div>
    <div class="container-fluid d-flex bg">
            <h1>Guía</h1>
            <div class="row">
                <div class="col-md-12">
                </br>
                <label class="label-color" for="">Ingresa el lugar a Visistar</label>
                <input type="text" name="" value="" id="buscar" class="form-control" placeholder="Buscar Lugar">
                <br>
                <button class="btn btn-success btn-lg btn-block" type="button" onclick="buscar()"  name="button">Buscar Lugar</button>
                <br>
            </div>
        </div>
                <h1 class="container-fluid link">Traza Tú Guía</h1>
            <form>
          <div class="row">
            <div class="col-md-6">
              <label class="label-color" for="">Inicio</label><input  id="rInicio" type="text" class="form-control" placeholder="Escriba dirección Inicio">
            </div>
            <div class="col-md-6">
              <label class="label-color" for="">Destino</label><input id="rDestino" type="text" class="form-control" placeholder="Escriba dirección Destino">
            </div>
          </div>
          </form>
      </br>
          <div class="container-fluid">
              <label class="label-color" for="">Elige Medio de Transporte</label>
              <select class="form-control" name="" id="modo">
                  <option selected="true" disabled="disabled">seleccione el medio</option>
                  <option value="DRIVING">Automóvil</option>
                  <option value="WALKING">Caminando</option>
              </select>
          </div>
      </br>
          <button class="btn btn-warning btn-lg btn-block" type="button" onclick="dibujarRuta()"  name="button">Trazar Guía</button>
      </br>
      <div class="container-fluid">
      <div id="map">
    </div>
    </br>
    </div>

      </div>

        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBLNm0QoB3sTjrmrSmIt9hgqeTgAkcnN-g&callback=iniciarMapa"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/gmaps.js/0.4.25/gmaps.js"></script>
        <script type="text/javascript" src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="../bower_components/jquery/dist/jquery.min.js"></script>
        <script type="text/javascript" src="../bower_components/jquery/dist/jquery.min.map.js"></script>
</div>
@endsection
@section('scripts')
@parent
<script>
var map = ""


function buscar(){

  //poner marker con una direccion
  var markerDomicilio = new google.maps.Geocoder();
  var domicilio =  document.getElementById("buscar").value;



  var objInformacion = {
    address: domicilio
  }
      markerDdomicilio = markerDomicilio.geocode(objInformacion, fn_coder);
      function fn_coder(datos){
        var coordenadas = datos[0].geometry.location; //objeto con latitud y lon
        var icon = {
          url: "plazaarmas.jpg",
          scaledSize: new google.maps.Size(50, 50), // scaled size
          origin: new google.maps.Point(0,0), // Origin
          anchor: new google.maps.Point(0, 0) // anchor
        }

        var config = {
          position: coordenadas,
          map: map,
          title: "Plaza 2"
        }
        var markerDom = new google.maps.Marker(config);
            markerDom.setIcon(icon);

            var configuracionMapa = {
              zoom: 17,
              center: coordenadas //es para centrar el mapa (si quieres que se centre en tu ubicacion cambia la variabñe por pos)
            }
            //fin datos de configuracion

            //El id map es el contenedor que donde estara el mapa
            map = new google.maps.Map(document.getElementById("map"),configuracionMapa);

            var marker  = new google.maps.Marker({
              position: coordenadas,
              map: map,
              title: "Catedral"
            });

      }




}//Fin Buscar

// Esta funcion la llama el script del api Key que esta en el HTML
function iniciarMapa(){

  //la linea 6 sirve para obtener la ubicaciondel usuario
  //recibe dos funciones como parametros
  //configurar mapa se ejecuta si el usuario aprobo  compartir la ubicaciondel
  //fn_error se ejecuta si el usuario rechazo compartir la ubicacion
  navigator.geolocation.getCurrentPosition(configurarMapa, fn_err);
}

//respuesta (argumento de la funcion )es el objeto que
//trae los datos de la ubicacion y se lo envia
//automaticamente la linea 9
function configurarMapa(respuesta){

 //se accede a la latitud y longitud
 //que vienen en el objeto respuesta
  var lat  = respuesta.coords.latitude;
  var lon = respuesta.coords.longitude;

  // se crea un objeto con los valores de la latitud y longitud
  // en la variable pos
  var pos = {lat:lat ,lng: lon};
  var catedral = {lat:19.702423, lng: -101.1923185}

  //configurar mapa

  //datos de configuracion
  var configuracionMapa = {
    zoom: 17,
    center: catedral //es para centrar el mapa (si quieres que se centre en tu ubicacion cambia la variabñe por pos)
  }
  //fin datos de configuracion

  //El id map es el contenedor que donde estara el mapa
  map = new google.maps.Map(document.getElementById("map"),configuracionMapa);
  //Fin configurar mapa


  ponerMarkers(map);
}
//Fin function my position





function ponerMarkers(){

  //Objeto que tiene las cordenadas de la catedral
  //de morelia

  var catedral = {lat:19.702423, lng: -101.1923185}
  var plazaArmas = {lat: 19.702896 ,lng: -101.190384};


  var marker  = new google.maps.Marker({
    position: catedral,
    map: map,
    title: "Catedral"
  });

  var marker2  = new google.maps.Marker({
    position: plazaArmas,
    map: map,
    title: "Plaza"
  });

  var objHTML = {
    content: "<h1>Hola</h1>"
  }
//google iw = google Info Window
var googleIW = new google.maps.InfoWindow(objHTML);
    google.maps.event.addListener(marker2, "click", function(){
    googleIW.open(map, marker2);
});



} //Fin poner Markers

//Fin funcion poner marker



function dibujarRuta(){

  var inicio = document.getElementById("rInicio").value;
  var destino = document.getElementById("rDestino").value;
  var listaModos = document.getElementById("modo");

  var modo = listaModos.options[listaModos.selectedIndex].value;

  var objConfigDR = {
    map: map,
    suppressMarkers: true
  }

  if(modo=="DRIVING"){
    var objConfigDS = {
      origin: inicio,
      destination: destino,
      travelMode: google.maps.TravelMode.DRIVING
    }
  }else{
    var objConfigDS = {
      origin: inicio,
      destination: destino,
      travelMode: google.maps.TravelMode.WALKING
    }
  }



  var ds = new google.maps.DirectionsService();
  var dr = new google.maps.DirectionsRenderer(objConfigDR);

  ds.route(objConfigDS, dibujarRuta);

  function dibujarRuta(resultados,status){
    //Muestra la ruta

    if(status== 'OK'){
      dr.setDirections(resultados);
    }else{
      alert('Error ' +status);
    }

  }








} //Fin Funcion Trazar Rutas







//si el usuario no acepta que la
//pagina lea la ubicacion se ejecuta esta
//funcion
function fn_err(){

}

</script>
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('guium_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}';
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.guia.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).data(), function (entry) {
          return entry.id
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  let dtOverrideGlobals = {
    buttons: dtButtons,
    processing: true,
    serverSide: true,
    retrieve: true,
    aaSorting: [],
    ajax: "{{ route('admin.guia.index') }}",
    columns: [
      { data: 'placeholder', name: 'placeholder' },
{ data: 'id', name: 'id' },
{ data: 'nombre', name: 'nombre' },
{ data: 'usuario_email', name: 'usuario.email' },
{ data: 'usuario.name', name: 'usuario.name' },
{ data: 'actions', name: '{{ trans('global.actions') }}' }
    ],
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  };
  $('.datatable-Guium').DataTable(dtOverrideGlobals);
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
});

</script>
@endsection

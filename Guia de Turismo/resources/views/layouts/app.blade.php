<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ trans('panel.site_title') }}</title>
    <link href="../css/bootstrap.min.css" type="text/css" rel="stylesheet" />
    <link href="../css/global.css" type="text/css" rel="stylesheet"/>
    <link href="../css/AdminLTE.min.css" type="text/css" rel="stylesheet" />
    <link type="text/css" rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">
     <link type="text/css" rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.css" />
     <link type="text/css" rel="stylesheet" href="../css/skins/_all-skins.min.css"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/all.css" rel="stylesheet" />

     <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />

    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,600,600i,700&display=swap&subset=latin-ext" rel="stylesheet" />
    @yield('styles')
    @laravelPWA
</head>

<body class="hold-transition login-page">
    <div class="container-fluid bg">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12"></div>
            <div class="col-md-4 col-sm-4 col-xs-12">
                @yield('content')

                <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
                <!--
                <script type="text/javascript" src="../bower_components/jquery/dist/jquery.min.js"></script>
                <script type="text/javascript" src="../js/icheck.min.js"></script>
                -->

                @yield('scripts')
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12"></div>
        </div>

    </div>
</body>
</html>

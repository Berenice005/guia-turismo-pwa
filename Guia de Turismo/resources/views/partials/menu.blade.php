<aside class="main-sidebar">
    <section class="sidebar" style="height: auto;">
        <div class="user-panel">
            <li class="header">
                <img src="../images/LG.jpeg" class="img-responsive" alt="User Image">
            </li>
            <li class="header"></li>
            <!--<div class="pull-left image">
                <img src="../images/LG.jpeg" class="img-responsive" alt="User Image">
            </div>-->
            <div class="pull-left">
                <div class="pull-left info">

                    <li><a href="#">{{ auth()->user()->name }}  - En Linea <i class="fa fa-circle text-success"></i> </a></li>
                </div>
            </div>
        </div>
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MENU DE NAVEGACIÓN</li>
            <li>
                <a href="{{ route("admin.home") }}">
                    <i class="fa fa-fw fa-dashboard ">

                    </i>
                    {{ trans('global.dashboard') }}
                </a>
            </li>
            @can('user_management_access')
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-fw fas fa-users">

                        </i>
                        <span>{{ trans('cruds.userManagement.title') }}</span>
                        <span class="pull-right-container"><i class="fa fa-fw fa-angle-left pull-right"></i></span>
                    </a>
                    <ul class="treeview-menu">
                        @can('permission_access')
                            <li class="{{ request()->is('admin/permissions') || request()->is('admin/permissions/*') ? 'active' : '' }}">
                                <a href="{{ route("admin.permissions.index") }}">
                                    <i class="fa fa-fw fas fa-unlock-alt">

                                    </i>
                                    <span>{{ trans('cruds.permission.title') }}</span>
                                </a>
                            </li>
                        @endcan
                        @can('role_access')
                            <li class="{{ request()->is('admin/roles') || request()->is('admin/roles/*') ? 'active' : '' }}">
                                <a href="{{ route("admin.roles.index") }}">
                                    <i class="fa fa-fw fas fa-briefcase">

                                    </i>
                                    <span>{{ trans('cruds.role.title') }}</span>
                                </a>
                            </li>
                        @endcan
                        @can('user_access')
                            <li class="{{ request()->is('admin/users') || request()->is('admin/users/*') ? 'active' : '' }}">
                                <a href="{{ route("admin.users.index") }}">
                                    <i class="fa fa-fw fas fa-user">

                                    </i>
                                    <span>{{ trans('cruds.user.title') }}</span>
                                </a>
                            </li>
                        @endcan
                    </ul>
                </li>
            @endcan
            @can('user_alert_access')
                <li class="{{ request()->is('admin/user-alerts') || request()->is('admin/user-alerts/*') ? 'active' : '' }}">
                    <a href="{{ route("admin.user-alerts.index") }}">
                        <i class="fa fa-fw fas fa-bell">

                        </i>
                        <span>{{ trans('cruds.userAlert.title') }}</span>
                    </a>
                </li>
            @endcan
            @can('expense_management_access')
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-fw fas fa-money">

                        </i>
                        <span>{{ trans('cruds.expenseManagement.title') }}</span>
                        <span class="pull-right-container"><i class="fa fa-fw fa-angle-left pull-right"></i></span>
                    </a>
                    <ul class="treeview-menu">
                        @can('expense_category_access')
                            <li class="{{ request()->is('admin/expense-categories') || request()->is('admin/expense-categories/*') ? 'active' : '' }}">
                                <a href="{{ route("admin.expense-categories.index") }}">
                                    <i class="fa fa-fw fas fa-list">

                                    </i>
                                    <span>{{ trans('cruds.expenseCategory.title') }}</span>
                                </a>
                            </li>
                        @endcan
                        @can('income_category_access')
                            <li class="{{ request()->is('admin/income-categories') || request()->is('admin/income-categories/*') ? 'active' : '' }}">
                                <a href="{{ route("admin.income-categories.index") }}">
                                    <i class="fa fa-fw fas fa-list">

                                    </i>
                                    <span>{{ trans('cruds.incomeCategory.title') }}</span>
                                </a>
                            </li>
                        @endcan
                        @can('expense_access')
                            <li class="{{ request()->is('admin/expenses') || request()->is('admin/expenses/*') ? 'active' : '' }}">
                                <a href="{{ route("admin.expenses.index") }}">
                                    <i class="fa fa-fw fas fa-arrow-circle-right">

                                    </i>
                                    <span>{{ trans('cruds.expense.title') }}</span>
                                </a>
                            </li>
                        @endcan
                        @can('income_access')
                            <li class="{{ request()->is('admin/incomes') || request()->is('admin/incomes/*') ? 'active' : '' }}">
                                <a href="{{ route("admin.incomes.index") }}">
                                    <i class="fa fa-fw fas fa-arrow-circle-right">

                                    </i>
                                    <span>{{ trans('cruds.income.title') }}</span>
                                </a>
                            </li>
                        @endcan
                        @can('expense_report_access')
                            <li class="{{ request()->is('admin/expense-reports') || request()->is('admin/expense-reports/*') ? 'active' : '' }}">
                                <a href="{{ route("admin.expense-reports.index") }}">
                                    <i class="fa fa-fw fas fa-line-chart">

                                    </i>
                                    <span>{{ trans('cruds.expenseReport.title') }}</span>
                                </a>
                            </li>
                        @endcan
                    </ul>
                </li>
            @endcan
            @can('faq_management_access')
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-fw fas fa-question">

                        </i>
                        <span>{{ trans('cruds.faqManagement.title') }}</span>
                        <span class="pull-right-container"><i class="fa fa-fw fa-angle-left pull-right"></i></span>
                    </a>
                    <ul class="treeview-menu">
                        @can('faq_category_access')
                            <li class="{{ request()->is('admin/faq-categories') || request()->is('admin/faq-categories/*') ? 'active' : '' }}">
                                <a href="{{ route("admin.faq-categories.index") }}">
                                    <i class="fa fa-fw fas fa-briefcase">

                                    </i>
                                    <span>{{ trans('cruds.faqCategory.title') }}</span>
                                </a>
                            </li>
                        @endcan
                        @can('faq_question_access')
                            <li class="{{ request()->is('admin/faq-questions') || request()->is('admin/faq-questions/*') ? 'active' : '' }}">
                                <a href="{{ route("admin.faq-questions.index") }}">
                                    <i class="fa fa-fw fas fa-question">

                                    </i>
                                    <span>{{ trans('cruds.faqQuestion.title') }}</span>
                                </a>
                            </li>
                        @endcan
                    </ul>
                </li>
            @endcan
            @can('contact_management_access')
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-fw fas fa-phone-square">

                        </i>
                        <span>{{ trans('cruds.contactManagement.title') }}</span>
                        <span class="pull-right-container"><i class="fa fa-fw fa-angle-left pull-right"></i></span>
                    </a>
                    <ul class="treeview-menu">
                        @can('contact_company_access')
                            <li class="{{ request()->is('admin/contact-companies') || request()->is('admin/contact-companies/*') ? 'active' : '' }}">
                                <a href="{{ route("admin.contact-companies.index") }}">
                                    <i class="fa fa-fw fas fa-building">

                                    </i>
                                    <span>{{ trans('cruds.contactCompany.title') }}</span>
                                </a>
                            </li>
                        @endcan
                        @can('contact_contact_access')
                            <li class="{{ request()->is('admin/contact-contacts') || request()->is('admin/contact-contacts/*') ? 'active' : '' }}">
                                <a href="{{ route("admin.contact-contacts.index") }}">
                                    <i class="fa fa-fw fas fa-user-plus">

                                    </i>
                                    <span>{{ trans('cruds.contactContact.title') }}</span>
                                </a>
                            </li>
                        @endcan
                    </ul>
                </li>
            @endcan
            @can('lugare_access')
                <li class="{{ request()->is('admin/lugares') || request()->is('admin/lugares/*') ? 'active' : '' }}">
                    <a href="{{ route("admin.lugares.index") }}">
                        <i class="fa fa-fw fas fa-map-marker">

                        </i>
                        <span>{{ trans('cruds.lugare.title') }}</span>
                    </a>
                </li>
            @endcan
            @can('experiencium_access')
                <li class="{{ request()->is('admin/experiencia') || request()->is('admin/experiencia/*') ? 'active' : '' }}">
                    <a href="{{ route("admin.experiencia.index") }}">
                        <i class="fa fa-fw fas fa-star">

                        </i>
                        <span>{{ trans('cruds.experiencium.title') }}</span>
                    </a>
                </li>
            @endcan
            @can('guium_access')
                <li class="{{ request()->is('admin/guia') || request()->is('admin/guia/*') ? 'active' : '' }}">
                    <a href="{{ route("admin.guia.index") }}">
                        <i class="fa fa-fw fas fa-map-pin">

                        </i>
                        <span>{{ trans('cruds.guium.title') }}</span>
                    </a>
                </li>
            @endcan
            @can('guia_lugare_access')
                <li class="{{ request()->is('admin/guia-lugares') || request()->is('admin/guia-lugares/*') ? 'active' : '' }}">
                    <a href="{{ route("admin.guia-lugares.index") }}">
                        <i class="fa fa-fw fas fa-bookmark">

                        </i>
                        <span>{{ trans('cruds.guiaLugare.title') }}</span>
                    </a>
                </li>
            @endcan
            @can('estado_guium_access')
                <li class="{{ request()->is('admin/estado-guia') || request()->is('admin/estado-guia/*') ? 'active' : '' }}">
                    <a href="{{ route("admin.estado-guia.index") }}">
                        <i class="fa fa-fw fas  fa-map">

                        </i>
                        <span>{{ trans('cruds.estadoGuium.title') }}</span>
                    </a>
                </li>
            @endcan
            @can('promocione_access')
                <li class="{{ request()->is('admin/promociones') || request()->is('admin/promociones/*') ? 'active' : '' }}">
                    <a href="{{ route("admin.promociones.index") }}">
                        <i class="fa fa-fw fas  fa-ticket">

                        </i>
                        <span>{{ trans('cruds.promocione.title') }}</span>
                    </a>
                </li>
            @endcan
            <li class="{{ request()->is('admin/system-calendar') || request()->is('admin/system-calendar/*') ? 'active' : '' }}">
                <a href="{{ route("admin.systemCalendar") }}">
                    <i class="fa fas fa-fw fa-calendar">

                    </i>
                    <span>{{ trans('global.systemCalendar') }}</span>
                </a>
            </li>
            @php($unread = \App\QaTopic::unreadCount())
                <li class="{{ request()->is('admin/messenger') || request()->is('admin/messenger/*') ? 'active' : '' }}">
                    <a href="{{ route("admin.messenger.index") }}">
                        <i class="fa fa-fw fa fa-envelope">

                        </i>
                        <span>{{ trans('global.messages') }}</span>
                        @if($unread > 0)
                            <strong>( {{ $unread }} )</strong>
                        @endif
                    </a>
                </li>
                @if(file_exists(app_path('Http/Controllers/Auth/ChangePasswordController.php')))
                    @can('profile_password_edit')
                        <li class="{{ request()->is('profile/password') || request()->is('profile/password/*') ? 'active' : '' }}">
                            <a href="{{ route('profile.password.edit') }}">
                                <i class="fa fa-fw fas fa-key">
                                </i>
                                {{ trans('global.change_password') }}
                            </a>
                        </li>
                    @endcan
                @endif
                <li>
                    <a href="#" onclick="event.preventDefault(); document.getElementById('logoutform').submit();">
                        <i class="fa fas fa-fw fa-sign-out">

                        </i>
                        {{ trans('global.logout') }}
                    </a>
                </li>
        </ul>
    </section>
</aside>

@extends('layouts.app')
@section('content')
        <div class="color">
            <a class="link" href="{{ route('admin.home') }}"></a>
                Guía de Turismo
   
        </div>
        @if(session('message'))
            <p class="alert alert-info">
                {{ session('message') }}
            </p>
        @endif
       

        <form class="form-container" method="POST" action="{{ route('login') }}">
            <h1 class="color">
                Inicia Sesión
            </h1>
            @csrf

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <input id="email" type="email" name="email" class="form-control" required autocomplete="email" autofocus placeholder="{{ trans('global.login_email') }}" value="{{ old('email', null) }}">

                @if($errors->has('email'))
                    <p class="help-block">
                        {{ $errors->first('email') }}
                    </p>
                @endif
            </div>
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <input id="password" type="password" name="password" class="form-control" required placeholder="{{ trans('global.login_password') }}">

                @if($errors->has('password'))
                    <p class="help-block">
                        {{ $errors->first('password') }}
                    </p>
                @endif
            </div>
        </br>
        <div class="row">
                <button type="submit" class="btn btn btn-warning btn-block">
                    {{ trans('global.login') }}
                </button>
        </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="checkbox icheck">
                        <label><input type="checkbox" name="remember"> {{ trans('global.remember_me') }}</label>
                    </div>
                </div>
            </div>
           </br>
                No tienes cuenta? <a class="link" href="{{ route('register') }}">{{ trans('global.register') }}</a>
            </br>
        </form>

@endsection

@section('scripts')
<script>
    $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>

@endsection

@extends('layouts.app')
@section('content')

    <div class="color">
        <a class="link" href="{{ route('admin.home') }}">
            Guía de Turismo
        </a>
    </div>
        <form class="form-container" method="POST" action="{{ route('register') }}">
            <h1>
                Nuevo Registro
            </h1>
            {{ csrf_field() }}
            <div>
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <input type="text" name="name" class="form-control" required autofocus placeholder="{{ trans('global.user_name') }}" value="{{ old('name', null) }}">
                    @if($errors->has('name'))
                        <p class="help-block">
                            {{ $errors->first('name') }}
                        </p>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input type="email" name="email" class="form-control" required placeholder="{{ trans('global.login_email') }}" value="{{ old('email', null) }}">
                    @if($errors->has('email'))
                        <p class="help-block">
                            {{ $errors->first('email') }}
                        </p>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <input type="password" name="password" class="form-control" required placeholder="{{ trans('global.login_password') }}">
                    @if($errors->has('password'))
                        <p class="help-block">
                            {{ $errors->first('password') }}
                        </p>
                    @endif
                </div>
                <div class="form-group">
                    <input type="password" name="password_confirmation" class="form-control" required placeholder="{{ trans('global.login_password_confirmation') }}">
                </div>
                <div class="row">
                        <button type="submit" class="btn btn btn-warning btn-block">
                            {{ trans('global.registernew') }}
                        </button>
                    </br>
                    Ya tienes cuenta?  <a class="link" href="{{ route('login') }}">{{ trans('global.login-regreso') }}</a>
                </div>
            </div>
        </form>

@endsection

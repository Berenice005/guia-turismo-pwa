<?php

namespace App\Http\Controllers\Admin;

use App\Guium;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\CsvImportTrait;
use App\Http\Requests\MassDestroyGuiumRequest;
use App\Http\Requests\StoreGuiumRequest;
use App\Http\Requests\UpdateGuiumRequest;
use App\User;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class GuiaController extends Controller
{
    use CsvImportTrait;

    public function index(Request $request)
    {
        abort_if(Gate::denies('guium_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            $query = Guium::with(['usuario'])->select(sprintf('%s.*', (new Guium)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'guium_show';
                $editGate      = 'guium_edit';
                $deleteGate    = 'guium_delete';
                $crudRoutePart = 'guia';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->editColumn('nombre', function ($row) {
                return $row->nombre ? $row->nombre : "";
            });
            $table->addColumn('usuario_email', function ($row) {
                return $row->usuario ? $row->usuario->email : '';
            });

            $table->editColumn('usuario.name', function ($row) {
                return $row->usuario ? (is_string($row->usuario) ? $row->usuario : $row->usuario->name) : '';
            });

            $table->rawColumns(['actions', 'placeholder', 'usuario']);

            return $table->make(true);
        }

        return view('admin.guia.index');
    }

    public function create()
    {
        abort_if(Gate::denies('guium_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $usuarios = User::all()->pluck('email', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.guia.create', compact('usuarios'));
    }

    public function store(StoreGuiumRequest $request)
    {
        $guium = Guium::create($request->all());

        return redirect()->route('admin.guia.index');
    }

    public function edit(Guium $guium)
    {
        abort_if(Gate::denies('guium_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $usuarios = User::all()->pluck('email', 'id')->prepend(trans('global.pleaseSelect'), '');

        $guium->load('usuario');

        return view('admin.guia.edit', compact('usuarios', 'guium'));
    }

    public function update(UpdateGuiumRequest $request, Guium $guium)
    {
        $guium->update($request->all());

        return redirect()->route('admin.guia.index');
    }

    public function show(Guium $guium)
    {
        abort_if(Gate::denies('guium_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $guium->load('usuario');

        return view('admin.guia.show', compact('guium'));
    }

    public function destroy(Guium $guium)
    {
        abort_if(Gate::denies('guium_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $guium->delete();

        return back();
    }

    public function massDestroy(MassDestroyGuiumRequest $request)
    {
        Guium::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}

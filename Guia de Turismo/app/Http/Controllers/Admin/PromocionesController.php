<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\CsvImportTrait;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyPromocioneRequest;
use App\Http\Requests\StorePromocioneRequest;
use App\Http\Requests\UpdatePromocioneRequest;
use App\Lugare;
use App\Promocione;
use Gate;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\Models\Media;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class PromocionesController extends Controller
{
    use MediaUploadingTrait, CsvImportTrait;

    public function index(Request $request)
    {
        abort_if(Gate::denies('promocione_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            $query = Promocione::with(['lugar'])->select(sprintf('%s.*', (new Promocione)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'promocione_show';
                $editGate      = 'promocione_edit';
                $deleteGate    = 'promocione_delete';
                $crudRoutePart = 'promociones';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->editColumn('nombre', function ($row) {
                return $row->nombre ? $row->nombre : "";
            });
            $table->addColumn('lugar_nombre', function ($row) {
                return $row->lugar ? $row->lugar->nombre : '';
            });

            $table->editColumn('fotopromo', function ($row) {
                if ($photo = $row->fotopromo) {
                    return sprintf(
                        '<a href="%s" target="_blank"><img src="%s" width="50px" height="50px"></a>',
                        $photo->url,
                        $photo->thumbnail
                    );
                }

                return '';
            });

            $table->rawColumns(['actions', 'placeholder', 'lugar', 'fotopromo']);

            return $table->make(true);
        }

        return view('admin.promociones.index');
    }

    public function create()
    {
        abort_if(Gate::denies('promocione_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $lugars = Lugare::all()->pluck('nombre', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.promociones.create', compact('lugars'));
    }

    public function store(StorePromocioneRequest $request)
    {
        $promocione = Promocione::create($request->all());

        if ($request->input('fotopromo', false)) {
            $promocione->addMedia(storage_path('tmp/uploads/' . $request->input('fotopromo')))->toMediaCollection('fotopromo');
        }

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $promocione->id]);
        }

        return redirect()->route('admin.promociones.index');
    }

    public function edit(Promocione $promocione)
    {
        abort_if(Gate::denies('promocione_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $lugars = Lugare::all()->pluck('nombre', 'id')->prepend(trans('global.pleaseSelect'), '');

        $promocione->load('lugar');

        return view('admin.promociones.edit', compact('lugars', 'promocione'));
    }

    public function update(UpdatePromocioneRequest $request, Promocione $promocione)
    {
        $promocione->update($request->all());

        if ($request->input('fotopromo', false)) {
            if (!$promocione->fotopromo || $request->input('fotopromo') !== $promocione->fotopromo->file_name) {
                $promocione->addMedia(storage_path('tmp/uploads/' . $request->input('fotopromo')))->toMediaCollection('fotopromo');
            }
        } elseif ($promocione->fotopromo) {
            $promocione->fotopromo->delete();
        }

        return redirect()->route('admin.promociones.index');
    }

    public function show(Promocione $promocione)
    {
        abort_if(Gate::denies('promocione_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $promocione->load('lugar');

        return view('admin.promociones.show', compact('promocione'));
    }

    public function destroy(Promocione $promocione)
    {
        abort_if(Gate::denies('promocione_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $promocione->delete();

        return back();
    }

    public function massDestroy(MassDestroyPromocioneRequest $request)
    {
        Promocione::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('promocione_create') && Gate::denies('promocione_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new Promocione();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);
    }
}

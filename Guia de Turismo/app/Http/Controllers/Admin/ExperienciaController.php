<?php

namespace App\Http\Controllers\Admin;

use App\Experiencium;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\CsvImportTrait;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyExperienciumRequest;
use App\Http\Requests\StoreExperienciumRequest;
use App\Http\Requests\UpdateExperienciumRequest;
use App\Lugare;
use App\User;
use Gate;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\Models\Media;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class ExperienciaController extends Controller
{
    use MediaUploadingTrait, CsvImportTrait;

    public function index(Request $request)
    {
        abort_if(Gate::denies('experiencium_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            $query = Experiencium::with(['usuario', 'lugar'])->select(sprintf('%s.*', (new Experiencium)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'experiencium_show';
                $editGate      = 'experiencium_edit';
                $deleteGate    = 'experiencium_delete';
                $crudRoutePart = 'experiencia';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->addColumn('usuario_email', function ($row) {
                return $row->usuario ? $row->usuario->email : '';
            });

            $table->editColumn('usuario.name', function ($row) {
                return $row->usuario ? (is_string($row->usuario) ? $row->usuario : $row->usuario->name) : '';
            });
            $table->addColumn('lugar_nombre', function ($row) {
                return $row->lugar ? $row->lugar->nombre : '';
            });

            $table->editColumn('calificacion', function ($row) {
                return $row->calificacion ? Experiencium::CALIFICACION_SELECT[$row->calificacion] : '';
            });

            $table->rawColumns(['actions', 'placeholder', 'usuario', 'lugar']);

            return $table->make(true);
        }

        return view('admin.experiencia.index');
    }

    public function create()
    {
        abort_if(Gate::denies('experiencium_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $usuarios = User::all()->pluck('email', 'id')->prepend(trans('global.pleaseSelect'), '');

        $lugars = Lugare::all()->pluck('nombre', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.experiencia.create', compact('usuarios', 'lugars'));
    }

    public function store(StoreExperienciumRequest $request)
    {
        $experiencium = Experiencium::create($request->all());

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $experiencium->id]);
        }

        return redirect()->route('admin.experiencia.index');
    }

    public function edit(Experiencium $experiencium)
    {
        abort_if(Gate::denies('experiencium_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $usuarios = User::all()->pluck('email', 'id')->prepend(trans('global.pleaseSelect'), '');

        $lugars = Lugare::all()->pluck('nombre', 'id')->prepend(trans('global.pleaseSelect'), '');

        $experiencium->load('usuario', 'lugar');

        return view('admin.experiencia.edit', compact('usuarios', 'lugars', 'experiencium'));
    }

    public function update(UpdateExperienciumRequest $request, Experiencium $experiencium)
    {
        $experiencium->update($request->all());

        return redirect()->route('admin.experiencia.index');
    }

    public function show(Experiencium $experiencium)
    {
        abort_if(Gate::denies('experiencium_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $experiencium->load('usuario', 'lugar');

        return view('admin.experiencia.show', compact('experiencium'));
    }

    public function destroy(Experiencium $experiencium)
    {
        abort_if(Gate::denies('experiencium_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $experiencium->delete();

        return back();
    }

    public function massDestroy(MassDestroyExperienciumRequest $request)
    {
        Experiencium::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('experiencium_create') && Gate::denies('experiencium_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new Experiencium();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);
    }
}

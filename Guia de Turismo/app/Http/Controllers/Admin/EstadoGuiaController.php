<?php

namespace App\Http\Controllers\Admin;

use App\EstadoGuium;
use App\Guium;
use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyEstadoGuiumRequest;
use App\Http\Requests\StoreEstadoGuiumRequest;
use App\Http\Requests\UpdateEstadoGuiumRequest;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class EstadoGuiaController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('estado_guium_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $estadoGuia = EstadoGuium::all();

        return view('admin.estadoGuia.index', compact('estadoGuia'));
    }

    public function create()
    {
        abort_if(Gate::denies('estado_guium_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $guias = Guium::all()->pluck('nombre', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.estadoGuia.create', compact('guias'));
    }

    public function store(StoreEstadoGuiumRequest $request)
    {
        $estadoGuium = EstadoGuium::create($request->all());

        return redirect()->route('admin.estado-guia.index');
    }

    public function edit(EstadoGuium $estadoGuium)
    {
        abort_if(Gate::denies('estado_guium_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $guias = Guium::all()->pluck('nombre', 'id')->prepend(trans('global.pleaseSelect'), '');

        $estadoGuium->load('guia');

        return view('admin.estadoGuia.edit', compact('guias', 'estadoGuium'));
    }

    public function update(UpdateEstadoGuiumRequest $request, EstadoGuium $estadoGuium)
    {
        $estadoGuium->update($request->all());

        return redirect()->route('admin.estado-guia.index');
    }

    public function show(EstadoGuium $estadoGuium)
    {
        abort_if(Gate::denies('estado_guium_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $estadoGuium->load('guia');

        return view('admin.estadoGuia.show', compact('estadoGuium'));
    }

    public function destroy(EstadoGuium $estadoGuium)
    {
        abort_if(Gate::denies('estado_guium_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $estadoGuium->delete();

        return back();
    }

    public function massDestroy(MassDestroyEstadoGuiumRequest $request)
    {
        EstadoGuium::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}

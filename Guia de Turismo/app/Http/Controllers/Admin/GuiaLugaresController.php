<?php

namespace App\Http\Controllers\Admin;

use App\GuiaLugare;
use App\Guium;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\CsvImportTrait;
use App\Http\Requests\MassDestroyGuiaLugareRequest;
use App\Http\Requests\StoreGuiaLugareRequest;
use App\Http\Requests\UpdateGuiaLugareRequest;
use App\Lugare;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class GuiaLugaresController extends Controller
{
    use CsvImportTrait;

    public function index(Request $request)
    {
        abort_if(Gate::denies('guia_lugare_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            $query = GuiaLugare::with(['guia', 'lugar'])->select(sprintf('%s.*', (new GuiaLugare)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'guia_lugare_show';
                $editGate      = 'guia_lugare_edit';
                $deleteGate    = 'guia_lugare_delete';
                $crudRoutePart = 'guia-lugares';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->addColumn('guia_nombre', function ($row) {
                return $row->guia ? $row->guia->nombre : '';
            });

            $table->addColumn('lugar_nombre', function ($row) {
                return $row->lugar ? $row->lugar->nombre : '';
            });

            $table->rawColumns(['actions', 'placeholder', 'guia', 'lugar']);

            return $table->make(true);
        }

        return view('admin.guiaLugares.index');
    }

    public function create()
    {
        abort_if(Gate::denies('guia_lugare_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $guias = Guium::all()->pluck('nombre', 'id')->prepend(trans('global.pleaseSelect'), '');

        $lugars = Lugare::all()->pluck('nombre', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.guiaLugares.create', compact('guias', 'lugars'));
    }

    public function store(StoreGuiaLugareRequest $request)
    {
        $guiaLugare = GuiaLugare::create($request->all());

        return redirect()->route('admin.guia-lugares.index');
    }

    public function edit(GuiaLugare $guiaLugare)
    {
        abort_if(Gate::denies('guia_lugare_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $guias = Guium::all()->pluck('nombre', 'id')->prepend(trans('global.pleaseSelect'), '');

        $lugars = Lugare::all()->pluck('nombre', 'id')->prepend(trans('global.pleaseSelect'), '');

        $guiaLugare->load('guia', 'lugar');

        return view('admin.guiaLugares.edit', compact('guias', 'lugars', 'guiaLugare'));
    }

    public function update(UpdateGuiaLugareRequest $request, GuiaLugare $guiaLugare)
    {
        $guiaLugare->update($request->all());

        return redirect()->route('admin.guia-lugares.index');
    }

    public function show(GuiaLugare $guiaLugare)
    {
        abort_if(Gate::denies('guia_lugare_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $guiaLugare->load('guia', 'lugar');

        return view('admin.guiaLugares.show', compact('guiaLugare'));
    }

    public function destroy(GuiaLugare $guiaLugare)
    {
        abort_if(Gate::denies('guia_lugare_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $guiaLugare->delete();

        return back();
    }

    public function massDestroy(MassDestroyGuiaLugareRequest $request)
    {
        GuiaLugare::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}

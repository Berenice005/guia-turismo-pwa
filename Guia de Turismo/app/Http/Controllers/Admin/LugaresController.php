<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\CsvImportTrait;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyLugareRequest;
use App\Http\Requests\StoreLugareRequest;
use App\Http\Requests\UpdateLugareRequest;
use App\Lugare;
use Gate;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\Models\Media;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class LugaresController extends Controller
{
    use MediaUploadingTrait, CsvImportTrait;

    public function index(Request $request)
    {
        abort_if(Gate::denies('lugare_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            $query = Lugare::query()->select(sprintf('%s.*', (new Lugare)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'lugare_show';
                $editGate      = 'lugare_edit';
                $deleteGate    = 'lugare_delete';
                $crudRoutePart = 'lugares';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->editColumn('nombre', function ($row) {
                return $row->nombre ? $row->nombre : "";
            });
            $table->editColumn('foto', function ($row) {
                if ($photo = $row->foto) {
                    return sprintf(
                        '<a href="%s" target="_blank"><img src="%s" width="50px" height="50px"></a>',
                        $photo->url,
                        $photo->thumbnail
                    );
                }

                return '';
            });
            $table->editColumn('latitud', function ($row) {
                return $row->latitud ? $row->latitud : "";
            });
            $table->editColumn('longitud', function ($row) {
                return $row->longitud ? $row->longitud : "";
            });

            $table->rawColumns(['actions', 'placeholder', 'foto']);

            return $table->make(true);
        }

        return view('admin.lugares.index');
    }

    public function create()
    {
        abort_if(Gate::denies('lugare_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.lugares.create');
    }

    public function store(StoreLugareRequest $request)
    {
        $lugare = Lugare::create($request->all());

        if ($request->input('foto', false)) {
            $lugare->addMedia(storage_path('tmp/uploads/' . $request->input('foto')))->toMediaCollection('foto');
        }

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $lugare->id]);
        }

        return redirect()->route('admin.lugares.index');
    }

    public function edit(Lugare $lugare)
    {
        abort_if(Gate::denies('lugare_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.lugares.edit', compact('lugare'));
    }

    public function update(UpdateLugareRequest $request, Lugare $lugare)
    {
        $lugare->update($request->all());

        if ($request->input('foto', false)) {
            if (!$lugare->foto || $request->input('foto') !== $lugare->foto->file_name) {
                $lugare->addMedia(storage_path('tmp/uploads/' . $request->input('foto')))->toMediaCollection('foto');
            }
        } elseif ($lugare->foto) {
            $lugare->foto->delete();
        }

        return redirect()->route('admin.lugares.index');
    }

    public function show(Lugare $lugare)
    {
        abort_if(Gate::denies('lugare_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.lugares.show', compact('lugare'));
    }

    public function destroy(Lugare $lugare)
    {
        abort_if(Gate::denies('lugare_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $lugare->delete();

        return back();
    }

    public function massDestroy(MassDestroyLugareRequest $request)
    {
        Lugare::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('lugare_create') && Gate::denies('lugare_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new Lugare();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);
    }
}

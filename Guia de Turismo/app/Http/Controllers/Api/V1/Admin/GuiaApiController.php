<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Guium;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreGuiumRequest;
use App\Http\Requests\UpdateGuiumRequest;
use App\Http\Resources\Admin\GuiumResource;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class GuiaApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('guium_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new GuiumResource(Guium::with(['usuario'])->get());
    }

    public function store(StoreGuiumRequest $request)
    {
        $guium = Guium::create($request->all());

        return (new GuiumResource($guium))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Guium $guium)
    {
        abort_if(Gate::denies('guium_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new GuiumResource($guium->load(['usuario']));
    }

    public function update(UpdateGuiumRequest $request, Guium $guium)
    {
        $guium->update($request->all());

        return (new GuiumResource($guium))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Guium $guium)
    {
        abort_if(Gate::denies('guium_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $guium->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}

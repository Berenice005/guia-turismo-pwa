<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\GuiaLugare;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreGuiaLugareRequest;
use App\Http\Requests\UpdateGuiaLugareRequest;
use App\Http\Resources\Admin\GuiaLugareResource;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class GuiaLugaresApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('guia_lugare_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new GuiaLugareResource(GuiaLugare::with(['guia', 'lugar'])->get());
    }

    public function store(StoreGuiaLugareRequest $request)
    {
        $guiaLugare = GuiaLugare::create($request->all());

        return (new GuiaLugareResource($guiaLugare))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(GuiaLugare $guiaLugare)
    {
        abort_if(Gate::denies('guia_lugare_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new GuiaLugareResource($guiaLugare->load(['guia', 'lugar']));
    }

    public function update(UpdateGuiaLugareRequest $request, GuiaLugare $guiaLugare)
    {
        $guiaLugare->update($request->all());

        return (new GuiaLugareResource($guiaLugare))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(GuiaLugare $guiaLugare)
    {
        abort_if(Gate::denies('guia_lugare_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $guiaLugare->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}

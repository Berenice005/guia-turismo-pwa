<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\StorePromocioneRequest;
use App\Http\Requests\UpdatePromocioneRequest;
use App\Http\Resources\Admin\PromocioneResource;
use App\Promocione;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class PromocionesApiController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('promocione_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new PromocioneResource(Promocione::with(['lugar'])->get());
    }

    public function store(StorePromocioneRequest $request)
    {
        $promocione = Promocione::create($request->all());

        if ($request->input('fotopromo', false)) {
            $promocione->addMedia(storage_path('tmp/uploads/' . $request->input('fotopromo')))->toMediaCollection('fotopromo');
        }

        return (new PromocioneResource($promocione))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Promocione $promocione)
    {
        abort_if(Gate::denies('promocione_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new PromocioneResource($promocione->load(['lugar']));
    }

    public function update(UpdatePromocioneRequest $request, Promocione $promocione)
    {
        $promocione->update($request->all());

        if ($request->input('fotopromo', false)) {
            if (!$promocione->fotopromo || $request->input('fotopromo') !== $promocione->fotopromo->file_name) {
                $promocione->addMedia(storage_path('tmp/uploads/' . $request->input('fotopromo')))->toMediaCollection('fotopromo');
            }
        } elseif ($promocione->fotopromo) {
            $promocione->fotopromo->delete();
        }

        return (new PromocioneResource($promocione))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Promocione $promocione)
    {
        abort_if(Gate::denies('promocione_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $promocione->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}

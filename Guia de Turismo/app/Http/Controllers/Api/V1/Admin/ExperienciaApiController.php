<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Experiencium;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\StoreExperienciumRequest;
use App\Http\Requests\UpdateExperienciumRequest;
use App\Http\Resources\Admin\ExperienciumResource;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ExperienciaApiController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('experiencium_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new ExperienciumResource(Experiencium::with(['usuario', 'lugar'])->get());
    }

    public function store(StoreExperienciumRequest $request)
    {
        $experiencium = Experiencium::create($request->all());

        return (new ExperienciumResource($experiencium))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Experiencium $experiencium)
    {
        abort_if(Gate::denies('experiencium_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new ExperienciumResource($experiencium->load(['usuario', 'lugar']));
    }

    public function update(UpdateExperienciumRequest $request, Experiencium $experiencium)
    {
        $experiencium->update($request->all());

        return (new ExperienciumResource($experiencium))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Experiencium $experiencium)
    {
        abort_if(Gate::denies('experiencium_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $experiencium->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}

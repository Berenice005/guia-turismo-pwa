<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\StoreLugareRequest;
use App\Http\Requests\UpdateLugareRequest;
use App\Http\Resources\Admin\LugareResource;
use App\Lugare;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class LugaresApiController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('lugare_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new LugareResource(Lugare::all());
    }

    public function store(StoreLugareRequest $request)
    {
        $lugare = Lugare::create($request->all());

        if ($request->input('foto', false)) {
            $lugare->addMedia(storage_path('tmp/uploads/' . $request->input('foto')))->toMediaCollection('foto');
        }

        return (new LugareResource($lugare))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Lugare $lugare)
    {
        abort_if(Gate::denies('lugare_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new LugareResource($lugare);
    }

    public function update(UpdateLugareRequest $request, Lugare $lugare)
    {
        $lugare->update($request->all());

        if ($request->input('foto', false)) {
            if (!$lugare->foto || $request->input('foto') !== $lugare->foto->file_name) {
                $lugare->addMedia(storage_path('tmp/uploads/' . $request->input('foto')))->toMediaCollection('foto');
            }
        } elseif ($lugare->foto) {
            $lugare->foto->delete();
        }

        return (new LugareResource($lugare))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Lugare $lugare)
    {
        abort_if(Gate::denies('lugare_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $lugare->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}

<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\EstadoGuium;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreEstadoGuiumRequest;
use App\Http\Requests\UpdateEstadoGuiumRequest;
use App\Http\Resources\Admin\EstadoGuiumResource;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class EstadoGuiaApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('estado_guium_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new EstadoGuiumResource(EstadoGuium::with(['guia'])->get());
    }

    public function store(StoreEstadoGuiumRequest $request)
    {
        $estadoGuium = EstadoGuium::create($request->all());

        return (new EstadoGuiumResource($estadoGuium))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(EstadoGuium $estadoGuium)
    {
        abort_if(Gate::denies('estado_guium_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new EstadoGuiumResource($estadoGuium->load(['guia']));
    }

    public function update(UpdateEstadoGuiumRequest $request, EstadoGuium $estadoGuium)
    {
        $estadoGuium->update($request->all());

        return (new EstadoGuiumResource($estadoGuium))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(EstadoGuium $estadoGuium)
    {
        abort_if(Gate::denies('estado_guium_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $estadoGuium->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}

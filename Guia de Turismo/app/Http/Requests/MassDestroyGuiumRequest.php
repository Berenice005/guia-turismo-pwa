<?php

namespace App\Http\Requests;

use App\Guium;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyGuiumRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('guium_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:guia,id',
        ];
    }
}

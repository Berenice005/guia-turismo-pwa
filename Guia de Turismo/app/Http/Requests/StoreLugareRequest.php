<?php

namespace App\Http\Requests;

use App\Lugare;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class StoreLugareRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('lugare_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'nombre'   => [
                'required',
                'unique:lugares',
            ],
            'foto'     => [
                'required',
            ],
            'latitud'  => [
                'required',
            ],
            'longitud' => [
                'required',
            ],
        ];
    }
}

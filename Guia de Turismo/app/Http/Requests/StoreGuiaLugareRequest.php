<?php

namespace App\Http\Requests;

use App\GuiaLugare;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class StoreGuiaLugareRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('guia_lugare_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'guia_id'  => [
                'required',
                'integer',
            ],
            'lugar_id' => [
                'required',
                'integer',
            ],
        ];
    }
}

<?php

namespace App\Http\Requests;

use App\GuiaLugare;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyGuiaLugareRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('guia_lugare_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:guia_lugares,id',
        ];
    }
}

<?php

namespace App\Http\Requests;

use App\Guium;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class StoreGuiumRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('guium_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'nombre'     => [
                'required',
                'unique:guia',
            ],
            'usuario_id' => [
                'required',
                'integer',
            ],
        ];
    }
}

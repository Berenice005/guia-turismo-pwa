<?php

namespace App\Http\Requests;

use App\Experiencium;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class StoreExperienciumRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('experiencium_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'usuario_id'   => [
                'required',
                'integer',
            ],
            'lugar_id'     => [
                'required',
                'integer',
            ],
            'calificacion' => [
                'required',
            ],
        ];
    }
}

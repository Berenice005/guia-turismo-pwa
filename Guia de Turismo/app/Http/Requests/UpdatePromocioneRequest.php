<?php

namespace App\Http\Requests;

use App\Promocione;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class UpdatePromocioneRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('promocione_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'nombre'       => [
                'required',
            ],
            'lugar_id'     => [
                'required',
                'integer',
            ],
            'descripcion'  => [
                'required',
            ],
            'fechainicio'  => [
                'required',
                'date_format:' . config('panel.date_format') . ' ' . config('panel.time_format'),
            ],
            'fechatermina' => [
                'required',
                'date_format:' . config('panel.date_format') . ' ' . config('panel.time_format'),
            ],
        ];
    }
}

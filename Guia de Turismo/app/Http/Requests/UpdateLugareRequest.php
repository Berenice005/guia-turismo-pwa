<?php

namespace App\Http\Requests;

use App\Lugare;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class UpdateLugareRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('lugare_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'nombre'   => [
                'required',
                'unique:lugares,nombre,' . request()->route('lugare')->id,
            ],
            'latitud'  => [
                'required',
            ],
            'longitud' => [
                'required',
            ],
        ];
    }
}

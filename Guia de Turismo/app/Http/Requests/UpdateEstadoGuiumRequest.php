<?php

namespace App\Http\Requests;

use App\EstadoGuium;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class UpdateEstadoGuiumRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('estado_guium_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'guia_id' => [
                'required',
                'integer',
            ],
            'estado'  => [
                'required',
            ],
        ];
    }
}

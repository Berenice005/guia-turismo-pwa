<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Experiencium extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait;

    public $table = 'experiencia';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'usuario_id',
        'comentario',
        'lugar_id',
        'calificacion',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    const CALIFICACION_SELECT = [
        '1' => 'Pésimo servicio.',
        '2' => 'Malo, no es como marca su descripción.',
        '3' => 'Regular, no me gusto la comida/ambiente.',
        '4' => 'Bueno, me gusto el servicio.',
        '5' => 'Excelente servicio, lo recomiendo.',
    ];

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->width(50)->height(50);
    }

    public function usuario()
    {
        return $this->belongsTo(User::class, 'usuario_id');
    }

    public function lugar()
    {
        return $this->belongsTo(Lugare::class, 'lugar_id');
    }
}

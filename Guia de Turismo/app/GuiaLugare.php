<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GuiaLugare extends Model
{
    use SoftDeletes;

    public $table = 'guia_lugares';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'guia_id',
        'lugar_id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function guia()
    {
        return $this->belongsTo(Guium::class, 'guia_id');
    }

    public function lugar()
    {
        return $this->belongsTo(Lugare::class, 'lugar_id');
    }
}

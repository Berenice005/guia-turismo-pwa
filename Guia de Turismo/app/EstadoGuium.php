<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EstadoGuium extends Model
{
    use SoftDeletes;

    public $table = 'estado_guia';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'guia_id',
        'estado',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    const ESTADO_SELECT = [
        '1' => 'Aceptado',
        '2' => 'En espera',
        '3' => 'Rechazado',
    ];

    public function guia()
    {
        return $this->belongsTo(Guium::class, 'guia_id');
    }
}

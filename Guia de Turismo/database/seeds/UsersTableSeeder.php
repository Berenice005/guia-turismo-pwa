<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $users = [
            [
                'id'                 => 1,
                'name'               => 'Admin',
                'email'              => 'admin@admin.com',
                'password'           => '$2y$10$9fKo45JfcgWWQSmWZ9JtueJ0mabUK4JUJMXKxzPRb.pfyy91YPd1i',
                'remember_token'     => null,
                'verified'           => 1,
                'verified_at'        => '2020-05-26 16:53:59',
                'ap_paterno'         => '',
                'ap_materno'         => '',
                'verification_token' => '',
            ],
        ];

        User::insert($users);
    }
}

<?php

use App\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    public function run()
    {
        $permissions = [
            [
                'id'    => '1',
                'title' => 'user_management_access',
            ],
            [
                'id'    => '2',
                'title' => 'permission_create',
            ],
            [
                'id'    => '3',
                'title' => 'permission_edit',
            ],
            [
                'id'    => '4',
                'title' => 'permission_show',
            ],
            [
                'id'    => '5',
                'title' => 'permission_delete',
            ],
            [
                'id'    => '6',
                'title' => 'permission_access',
            ],
            [
                'id'    => '7',
                'title' => 'role_create',
            ],
            [
                'id'    => '8',
                'title' => 'role_edit',
            ],
            [
                'id'    => '9',
                'title' => 'role_show',
            ],
            [
                'id'    => '10',
                'title' => 'role_delete',
            ],
            [
                'id'    => '11',
                'title' => 'role_access',
            ],
            [
                'id'    => '12',
                'title' => 'user_create',
            ],
            [
                'id'    => '13',
                'title' => 'user_edit',
            ],
            [
                'id'    => '14',
                'title' => 'user_show',
            ],
            [
                'id'    => '15',
                'title' => 'user_delete',
            ],
            [
                'id'    => '16',
                'title' => 'user_access',
            ],
            [
                'id'    => '17',
                'title' => 'user_alert_create',
            ],
            [
                'id'    => '18',
                'title' => 'user_alert_show',
            ],
            [
                'id'    => '19',
                'title' => 'user_alert_delete',
            ],
            [
                'id'    => '20',
                'title' => 'user_alert_access',
            ],
            [
                'id'    => '21',
                'title' => 'expense_management_access',
            ],
            [
                'id'    => '22',
                'title' => 'expense_category_create',
            ],
            [
                'id'    => '23',
                'title' => 'expense_category_edit',
            ],
            [
                'id'    => '24',
                'title' => 'expense_category_show',
            ],
            [
                'id'    => '25',
                'title' => 'expense_category_delete',
            ],
            [
                'id'    => '26',
                'title' => 'expense_category_access',
            ],
            [
                'id'    => '27',
                'title' => 'income_category_create',
            ],
            [
                'id'    => '28',
                'title' => 'income_category_edit',
            ],
            [
                'id'    => '29',
                'title' => 'income_category_show',
            ],
            [
                'id'    => '30',
                'title' => 'income_category_delete',
            ],
            [
                'id'    => '31',
                'title' => 'income_category_access',
            ],
            [
                'id'    => '32',
                'title' => 'expense_create',
            ],
            [
                'id'    => '33',
                'title' => 'expense_edit',
            ],
            [
                'id'    => '34',
                'title' => 'expense_show',
            ],
            [
                'id'    => '35',
                'title' => 'expense_delete',
            ],
            [
                'id'    => '36',
                'title' => 'expense_access',
            ],
            [
                'id'    => '37',
                'title' => 'income_create',
            ],
            [
                'id'    => '38',
                'title' => 'income_edit',
            ],
            [
                'id'    => '39',
                'title' => 'income_show',
            ],
            [
                'id'    => '40',
                'title' => 'income_delete',
            ],
            [
                'id'    => '41',
                'title' => 'income_access',
            ],
            [
                'id'    => '42',
                'title' => 'expense_report_create',
            ],
            [
                'id'    => '43',
                'title' => 'expense_report_edit',
            ],
            [
                'id'    => '44',
                'title' => 'expense_report_show',
            ],
            [
                'id'    => '45',
                'title' => 'expense_report_delete',
            ],
            [
                'id'    => '46',
                'title' => 'expense_report_access',
            ],
            [
                'id'    => '47',
                'title' => 'faq_management_access',
            ],
            [
                'id'    => '48',
                'title' => 'faq_category_create',
            ],
            [
                'id'    => '49',
                'title' => 'faq_category_edit',
            ],
            [
                'id'    => '50',
                'title' => 'faq_category_show',
            ],
            [
                'id'    => '51',
                'title' => 'faq_category_delete',
            ],
            [
                'id'    => '52',
                'title' => 'faq_category_access',
            ],
            [
                'id'    => '53',
                'title' => 'faq_question_create',
            ],
            [
                'id'    => '54',
                'title' => 'faq_question_edit',
            ],
            [
                'id'    => '55',
                'title' => 'faq_question_show',
            ],
            [
                'id'    => '56',
                'title' => 'faq_question_delete',
            ],
            [
                'id'    => '57',
                'title' => 'faq_question_access',
            ],
            [
                'id'    => '58',
                'title' => 'contact_management_access',
            ],
            [
                'id'    => '59',
                'title' => 'contact_company_create',
            ],
            [
                'id'    => '60',
                'title' => 'contact_company_edit',
            ],
            [
                'id'    => '61',
                'title' => 'contact_company_show',
            ],
            [
                'id'    => '62',
                'title' => 'contact_company_delete',
            ],
            [
                'id'    => '63',
                'title' => 'contact_company_access',
            ],
            [
                'id'    => '64',
                'title' => 'contact_contact_create',
            ],
            [
                'id'    => '65',
                'title' => 'contact_contact_edit',
            ],
            [
                'id'    => '66',
                'title' => 'contact_contact_show',
            ],
            [
                'id'    => '67',
                'title' => 'contact_contact_delete',
            ],
            [
                'id'    => '68',
                'title' => 'contact_contact_access',
            ],
            [
                'id'    => '69',
                'title' => 'lugare_create',
            ],
            [
                'id'    => '70',
                'title' => 'lugare_edit',
            ],
            [
                'id'    => '71',
                'title' => 'lugare_show',
            ],
            [
                'id'    => '72',
                'title' => 'lugare_delete',
            ],
            [
                'id'    => '73',
                'title' => 'lugare_access',
            ],
            [
                'id'    => '74',
                'title' => 'experiencium_create',
            ],
            [
                'id'    => '75',
                'title' => 'experiencium_edit',
            ],
            [
                'id'    => '76',
                'title' => 'experiencium_show',
            ],
            [
                'id'    => '77',
                'title' => 'experiencium_delete',
            ],
            [
                'id'    => '78',
                'title' => 'experiencium_access',
            ],
            [
                'id'    => '79',
                'title' => 'guium_create',
            ],
            [
                'id'    => '80',
                'title' => 'guium_edit',
            ],
            [
                'id'    => '81',
                'title' => 'guium_show',
            ],
            [
                'id'    => '82',
                'title' => 'guium_delete',
            ],
            [
                'id'    => '83',
                'title' => 'guium_access',
            ],
            [
                'id'    => '84',
                'title' => 'guia_lugare_create',
            ],
            [
                'id'    => '85',
                'title' => 'guia_lugare_edit',
            ],
            [
                'id'    => '86',
                'title' => 'guia_lugare_show',
            ],
            [
                'id'    => '87',
                'title' => 'guia_lugare_delete',
            ],
            [
                'id'    => '88',
                'title' => 'guia_lugare_access',
            ],
            [
                'id'    => '89',
                'title' => 'estado_guium_create',
            ],
            [
                'id'    => '90',
                'title' => 'estado_guium_edit',
            ],
            [
                'id'    => '91',
                'title' => 'estado_guium_show',
            ],
            [
                'id'    => '92',
                'title' => 'estado_guium_delete',
            ],
            [
                'id'    => '93',
                'title' => 'estado_guium_access',
            ],
            [
                'id'    => '94',
                'title' => 'promocione_create',
            ],
            [
                'id'    => '95',
                'title' => 'promocione_edit',
            ],
            [
                'id'    => '96',
                'title' => 'promocione_show',
            ],
            [
                'id'    => '97',
                'title' => 'promocione_delete',
            ],
            [
                'id'    => '98',
                'title' => 'promocione_access',
            ],
            [
                'id'    => '99',
                'title' => 'profile_password_edit',
            ],
        ];

        Permission::insert($permissions);
    }
}

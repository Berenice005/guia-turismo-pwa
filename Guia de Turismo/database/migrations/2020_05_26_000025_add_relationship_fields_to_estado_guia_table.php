<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToEstadoGuiaTable extends Migration
{
    public function up()
    {
        Schema::table('estado_guia', function (Blueprint $table) {
            $table->unsignedInteger('guia_id');
            $table->foreign('guia_id', 'guia_fk_1530575')->references('id')->on('guia');
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExperienciaTable extends Migration
{
    public function up()
    {
        Schema::create('experiencia', function (Blueprint $table) {
            $table->increments('id');
            $table->longText('comentario')->nullable();
            $table->string('calificacion');
            $table->timestamps();
            $table->softDeletes();
        });
    }
}

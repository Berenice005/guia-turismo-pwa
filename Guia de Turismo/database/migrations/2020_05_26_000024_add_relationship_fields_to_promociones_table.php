<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToPromocionesTable extends Migration
{
    public function up()
    {
        Schema::table('promociones', function (Blueprint $table) {
            $table->unsignedInteger('lugar_id');
            $table->foreign('lugar_id', 'lugar_fk_1530583')->references('id')->on('lugares');
        });
    }
}

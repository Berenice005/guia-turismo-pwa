<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToExperienciaTable extends Migration
{
    public function up()
    {
        Schema::table('experiencia', function (Blueprint $table) {
            $table->unsignedInteger('usuario_id');
            $table->foreign('usuario_id', 'usuario_fk_1530538')->references('id')->on('users');
            $table->unsignedInteger('lugar_id');
            $table->foreign('lugar_id', 'lugar_fk_1530540')->references('id')->on('lugares');
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLugaresTable extends Migration
{
    public function up()
    {
        Schema::create('lugares', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre')->unique();
            $table->longText('descripcion')->nullable();
            $table->string('latitud');
            $table->string('longitud');
            $table->timestamps();
            $table->softDeletes();
        });
    }
}

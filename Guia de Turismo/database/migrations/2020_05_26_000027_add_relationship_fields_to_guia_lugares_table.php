<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToGuiaLugaresTable extends Migration
{
    public function up()
    {
        Schema::table('guia_lugares', function (Blueprint $table) {
            $table->unsignedInteger('guia_id');
            $table->foreign('guia_id', 'guia_fk_1530560')->references('id')->on('guia');
            $table->unsignedInteger('lugar_id');
            $table->foreign('lugar_id', 'lugar_fk_1530561')->references('id')->on('lugares');
        });
    }
}

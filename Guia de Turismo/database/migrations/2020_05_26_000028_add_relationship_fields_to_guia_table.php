<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToGuiaTable extends Migration
{
    public function up()
    {
        Schema::table('guia', function (Blueprint $table) {
            $table->unsignedInteger('usuario_id');
            $table->foreign('usuario_id', 'usuario_fk_1530555')->references('id')->on('users');
        });
    }
}

<?php

Route::redirect('/', '/login');
Route::get('/offline', function () {    
    return view('vendor/laravelpwa/offline');
});
Route::get('/mapa', function () {    
    return view('/mapa');
});
Route::get('/ranking', function () {    
    return view('/ranking');
});
Route::get('/home', function () {
    if (session('status')) {
        return redirect()->route('admin.home')->with('status', session('status'));
    }

    return redirect()->route('admin.home');
});

Auth::routes();
Route::get('userVerification/{token}', 'UserVerificationController@approve')->name('userVerification');
// Admin

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('user-alerts/read', 'UserAlertsController@read');
    // Permissions
    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');
    Route::resource('permissions', 'PermissionsController');

    // Roles
    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');
    Route::resource('roles', 'RolesController');

    // Users
    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');
    Route::resource('users', 'UsersController');

    // User Alerts
    Route::delete('user-alerts/destroy', 'UserAlertsController@massDestroy')->name('user-alerts.massDestroy');
    Route::resource('user-alerts', 'UserAlertsController', ['except' => ['edit', 'update']]);

    // Expense Categories
    Route::delete('expense-categories/destroy', 'ExpenseCategoryController@massDestroy')->name('expense-categories.massDestroy');
    Route::resource('expense-categories', 'ExpenseCategoryController');

    // Income Categories
    Route::delete('income-categories/destroy', 'IncomeCategoryController@massDestroy')->name('income-categories.massDestroy');
    Route::resource('income-categories', 'IncomeCategoryController');

    // Expenses
    Route::delete('expenses/destroy', 'ExpenseController@massDestroy')->name('expenses.massDestroy');
    Route::resource('expenses', 'ExpenseController');

    // Incomes
    Route::delete('incomes/destroy', 'IncomeController@massDestroy')->name('incomes.massDestroy');
    Route::resource('incomes', 'IncomeController');

    // Expense Reports
    Route::delete('expense-reports/destroy', 'ExpenseReportController@massDestroy')->name('expense-reports.massDestroy');
    Route::resource('expense-reports', 'ExpenseReportController');

    // Faq Categories
    Route::delete('faq-categories/destroy', 'FaqCategoryController@massDestroy')->name('faq-categories.massDestroy');
    Route::resource('faq-categories', 'FaqCategoryController');

    // Faq Questions
    Route::delete('faq-questions/destroy', 'FaqQuestionController@massDestroy')->name('faq-questions.massDestroy');
    Route::resource('faq-questions', 'FaqQuestionController');

    // Contact Companies
    Route::delete('contact-companies/destroy', 'ContactCompanyController@massDestroy')->name('contact-companies.massDestroy');
    Route::resource('contact-companies', 'ContactCompanyController');

    // Contact Contacts
    Route::delete('contact-contacts/destroy', 'ContactContactsController@massDestroy')->name('contact-contacts.massDestroy');
    Route::resource('contact-contacts', 'ContactContactsController');

    // Lugares
    Route::delete('lugares/destroy', 'LugaresController@massDestroy')->name('lugares.massDestroy');
    Route::post('lugares/media', 'LugaresController@storeMedia')->name('lugares.storeMedia');
    Route::post('lugares/ckmedia', 'LugaresController@storeCKEditorImages')->name('lugares.storeCKEditorImages');
    Route::post('lugares/parse-csv-import', 'LugaresController@parseCsvImport')->name('lugares.parseCsvImport');
    Route::post('lugares/process-csv-import', 'LugaresController@processCsvImport')->name('lugares.processCsvImport');
    Route::resource('lugares', 'LugaresController');

    // Experiencia
    Route::delete('experiencia/destroy', 'ExperienciaController@massDestroy')->name('experiencia.massDestroy');
    Route::post('experiencia/media', 'ExperienciaController@storeMedia')->name('experiencia.storeMedia');
    Route::post('experiencia/ckmedia', 'ExperienciaController@storeCKEditorImages')->name('experiencia.storeCKEditorImages');
    Route::post('experiencia/parse-csv-import', 'ExperienciaController@parseCsvImport')->name('experiencia.parseCsvImport');
    Route::post('experiencia/process-csv-import', 'ExperienciaController@processCsvImport')->name('experiencia.processCsvImport');
    Route::resource('experiencia', 'ExperienciaController');

    // Guia
    Route::delete('guia/destroy', 'GuiaController@massDestroy')->name('guia.massDestroy');
    Route::post('guia/parse-csv-import', 'GuiaController@parseCsvImport')->name('guia.parseCsvImport');
    Route::post('guia/process-csv-import', 'GuiaController@processCsvImport')->name('guia.processCsvImport');
    Route::resource('guia', 'GuiaController');

    // Guia Lugares
    Route::delete('guia-lugares/destroy', 'GuiaLugaresController@massDestroy')->name('guia-lugares.massDestroy');
    Route::post('guia-lugares/parse-csv-import', 'GuiaLugaresController@parseCsvImport')->name('guia-lugares.parseCsvImport');
    Route::post('guia-lugares/process-csv-import', 'GuiaLugaresController@processCsvImport')->name('guia-lugares.processCsvImport');
    Route::resource('guia-lugares', 'GuiaLugaresController');

    // Estado Guia
    Route::delete('estado-guia/destroy', 'EstadoGuiaController@massDestroy')->name('estado-guia.massDestroy');
    Route::resource('estado-guia', 'EstadoGuiaController');

    // Promociones
    Route::delete('promociones/destroy', 'PromocionesController@massDestroy')->name('promociones.massDestroy');
    Route::post('promociones/media', 'PromocionesController@storeMedia')->name('promociones.storeMedia');
    Route::post('promociones/ckmedia', 'PromocionesController@storeCKEditorImages')->name('promociones.storeCKEditorImages');
    Route::post('promociones/parse-csv-import', 'PromocionesController@parseCsvImport')->name('promociones.parseCsvImport');
    Route::post('promociones/process-csv-import', 'PromocionesController@processCsvImport')->name('promociones.processCsvImport');
    Route::resource('promociones', 'PromocionesController');

    Route::get('system-calendar', 'SystemCalendarController@index')->name('systemCalendar');
    Route::get('messenger', 'MessengerController@index')->name('messenger.index');
    Route::get('messenger/create', 'MessengerController@createTopic')->name('messenger.createTopic');
    Route::post('messenger', 'MessengerController@storeTopic')->name('messenger.storeTopic');
    Route::get('messenger/inbox', 'MessengerController@showInbox')->name('messenger.showInbox');
    Route::get('messenger/outbox', 'MessengerController@showOutbox')->name('messenger.showOutbox');
    Route::get('messenger/{topic}', 'MessengerController@showMessages')->name('messenger.showMessages');
    Route::delete('messenger/{topic}', 'MessengerController@destroyTopic')->name('messenger.destroyTopic');
    Route::post('messenger/{topic}/reply', 'MessengerController@replyToTopic')->name('messenger.reply');
    Route::get('messenger/{topic}/reply', 'MessengerController@showReply')->name('messenger.showReply');
});
Route::group(['prefix' => 'profile', 'as' => 'profile.', 'namespace' => 'Auth', 'middleware' => ['auth']], function () {
// Change password
    if (file_exists(app_path('Http/Controllers/Auth/ChangePasswordController.php'))) {
        Route::get('password', 'ChangePasswordController@edit')->name('password.edit');
        Route::post('password', 'ChangePasswordController@update')->name('password.update');
    }
});

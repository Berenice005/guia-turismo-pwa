<?php

Route::group(['prefix' => 'v1', 'as' => 'api.', 'namespace' => 'Api\V1\Admin', 'middleware' => ['auth:api']], function () {
    // Permissions
    Route::apiResource('permissions', 'PermissionsApiController');

    // Roles
    Route::apiResource('roles', 'RolesApiController');

    // Users
    Route::apiResource('users', 'UsersApiController');

    // Expense Categories
    Route::apiResource('expense-categories', 'ExpenseCategoryApiController');

    // Income Categories
    Route::apiResource('income-categories', 'IncomeCategoryApiController');

    // Expenses
    Route::apiResource('expenses', 'ExpenseApiController');

    // Incomes
    Route::apiResource('incomes', 'IncomeApiController');

    // Faq Categories
    Route::apiResource('faq-categories', 'FaqCategoryApiController');

    // Faq Questions
    Route::apiResource('faq-questions', 'FaqQuestionApiController');

    // Contact Companies
    Route::apiResource('contact-companies', 'ContactCompanyApiController');

    // Contact Contacts
    Route::apiResource('contact-contacts', 'ContactContactsApiController');

    // Lugares
    Route::post('lugares/media', 'LugaresApiController@storeMedia')->name('lugares.storeMedia');
    Route::apiResource('lugares', 'LugaresApiController');

    // Experiencia
    Route::post('experiencia/media', 'ExperienciaApiController@storeMedia')->name('experiencia.storeMedia');
    Route::apiResource('experiencia', 'ExperienciaApiController');

    // Guia
    Route::apiResource('guia', 'GuiaApiController');

    // Guia Lugares
    Route::apiResource('guia-lugares', 'GuiaLugaresApiController');

    // Estado Guia
    Route::apiResource('estado-guia', 'EstadoGuiaApiController');

    // Promociones
    Route::post('promociones/media', 'PromocionesApiController@storeMedia')->name('promociones.storeMedia');
    Route::apiResource('promociones', 'PromocionesApiController');
});
